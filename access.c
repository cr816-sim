#include <string.h>
#include <stdlib.h>
#include "disasm.h"
#include "log.h"
#include "isa.h"
#include "ioregs.h"

static u8 mem_ram[0x800];
static u8 mem_flash[0x800];

static u16 trace_pc = 0;

//TODO use tracing for log RW
#define TRACE_REG_NUM 1000000
static unsigned trace_regs8_r_idx = 0;
static struct trace_regs8_r regs8_r_history[TRACE_REG_NUM];
static unsigned trace_regs8_w_idx = 0;
static struct trace_regs8_w regs8_w_history[TRACE_REG_NUM];
static struct trace_regs8_w regs8_curr[UTIL_REG_FLAGS+1];	//NOTICE last

#define TRACE_MEM_NUM 1000000
static unsigned trace_mem_r_idx = 0;
static unsigned trace_mem_w_idx = 0;
static struct trace_mem_r mem_r_history[TRACE_MEM_NUM];
static struct trace_mem_w mem_w_history[TRACE_MEM_NUM];


/** ****** access utils ******/

static u8 trace_add_read(
	u16 pc,
	unsigned reg_idx
)
{
	u8 ret;

	regs8_r_history[trace_regs8_r_idx].pc = pc;
	regs8_r_history[trace_regs8_r_idx].reg = reg_idx;

	trace_regs8_r_idx = (trace_regs8_r_idx+1)%TRACE_REG_NUM;

	ret = regs8_curr[reg_idx].raw;

	return ret;
}

static void trace_add_write(
	u16 pc,
	unsigned reg_idx,
	u8 val
)
{
	regs8_w_history[trace_regs8_w_idx].pc = pc;
	regs8_w_history[trace_regs8_w_idx].reg = reg_idx;
	regs8_w_history[trace_regs8_w_idx].raw = val;

	trace_regs8_w_idx = (trace_regs8_w_idx+1)%TRACE_REG_NUM;

	regs8_curr[reg_idx].raw = val;
}

//just read/write ... backtrace can be done in regs8_history[]
u16 read_pc(void)
{
	return trace_pc;
}

void write_pc(u16 addr)
{
	if (addr == (trace_pc+1) ) {
		trace_pc = addr;
	} else {
		trace_pc = addr;
		//log only jumps?
		log_access_write_add("pc=%04hx ", addr);
	}
}

u8 read_reg8(enum alu_regs reg, unsigned log)
{
	u8 ret;
	ret = trace_add_read(trace_pc, reg);

	if (log)
		log_access_read_add("%02hhx=%s ",
							ret, get_regs_name(reg));

	return ret;
}

void write_reg8(enum alu_regs reg, u8 val, unsigned log)
{
	trace_add_write(trace_pc, reg, val);

	if (log)
		log_access_write_add("%s=%02hhx ",
							 get_regs_name(reg), val);
	return ;
}
//
//maybe join with *reg functions
void write_flags(struct cpuflags cpuflags, unsigned log)
{

	trace_add_write(trace_pc, UTIL_REG_FLAGS, cpuflags.raw);

	if (log)
		log_access_write_add("flags=%02hhx(%c%c%c) ",
			cpuflags.raw,
			cpuflags.z?'Z':'z',
			cpuflags.c?'C':'c',
			cpuflags.v?'V':'v'
		);
}

struct cpuflags read_flags(unsigned log)
{
	struct cpuflags ret;

	ret.raw = trace_add_read(trace_pc, UTIL_REG_FLAGS);

	if (log)
		log_access_read_add("%02hhx(%c%c%c)=flags ",
			ret.raw,
			ret.z?'Z':'z',
			ret.c?'C':'c',
			ret.v?'V':'v'
		);

	return ret;
}


u16 read_ix(enum alu_ixs ixs, unsigned log)
{
	u16 ret;
	u8 lo, hi;

	switch(ixs) {
		case I_REG_0:
			lo = read_reg8(MAIN_REG_I0L, 0);
			hi = read_reg8(MAIN_REG_I0H, 0);
			break;
		case I_REG_1:
			lo = read_reg8(MAIN_REG_I1L, 0);
			hi = read_reg8(MAIN_REG_I1H, 0);
			break;
		case I_REG_2:
			lo = read_reg8(MAIN_REG_I2L, 0);
			hi = read_reg8(MAIN_REG_I2H, 0);
			break;
		case I_REG_3:
			lo = read_reg8(MAIN_REG_I3L, 0);
			hi = read_reg8(MAIN_REG_I3H, 0);
			break;
		default:
			fprintf(stderr, "!!error read_ix unknown reg\n");
			return 0xdead;
	}

	ret = lo | (hi << 8);

	if (log)
		log_access_read_add("%04hx=%s ", ret, get_ixs_name(ixs));

	return ret;
}


void write_ix(enum alu_ixs ixs, u16 val, unsigned log)
{
	u8 lo, hi;

	lo = val & 0xff;
	hi = (val >> 8) & 0xff;

	switch(ixs) {
		case I_REG_0:
			write_reg8(MAIN_REG_I0L, lo, 0);
			write_reg8(MAIN_REG_I0H, hi, 0);
			break;
		case I_REG_1:
			write_reg8(MAIN_REG_I1L, lo, 0);
			write_reg8(MAIN_REG_I1H, hi, 0);
			break;
		case I_REG_2:
			write_reg8(MAIN_REG_I2L, lo, 0);
			write_reg8(MAIN_REG_I2H, hi, 0);
			break;
		case I_REG_3:
			write_reg8(MAIN_REG_I3L, lo, 0);
			write_reg8(MAIN_REG_I3H, hi, 0);
			break;
		default:
			fprintf(stderr, "!!error write_ix unknown reg\n");
			return;
	}

	if (log)
		log_access_write_add("%s=%04hx ", get_ixs_name(ixs), val);
}

u16 read_ip(unsigned log)
{
	u16 ret;

	ret = (read_reg8(MAIN_REG_IPH, 0) << 8) |
			read_reg8(MAIN_REG_IPL, 0);

	if (log)
		log_access_read_add("%04hx=ip ", ret);

	return ret;
}


void write_ip(u16 val, unsigned log)
{
	write_reg8(MAIN_REG_IPL, val & 0xff, 0);
	write_reg8(MAIN_REG_IPH, (val >> 8) & 0xff, 0);

	if (log)
		log_access_write_add("ip=%04hx ", val);
}


u8 read_ram(u16 addr)
{
	//TODO watch special adresses?

	//NOTICE these may be inaccurate
	switch(addr) {
		case 0:
			log_comment_add("	smb_ctl\n");
			break;
		case 1:
			log_comment_add("	smb_errno\n");
			break;
		case 2:
			log_comment_add("	i2c_errno\n");
			break;
		case 5:
			log_comment_add("	process_list MSB (%04hx)\n", (mem_ram[5]<<8) | mem_ram[6]);
			break;
		case 6:
			log_comment_add("	process_list LSB (%04hx)\n", (mem_ram[5]<<8) | mem_ram[6]);
			break;
		case 7:
			log_comment_add("	process_ptr MSB (%04hx)\n", (mem_ram[7]<<8) | mem_ram[8]);
			break;
		case 8:
			log_comment_add("	process_ptr LSB (%04hx)\n", (mem_ram[7]<<8) | mem_ram[8]);
			break;
		case 9:
			log_comment_add("	num processes = %hhd\n", mem_ram[addr]);
			break;
		case 0xa:
			log_comment_add("	halt_mode = %hhd\n", mem_ram[addr]);
			log_comment_add("		0 (or other) = set gie, don't touch anything, halt\n");
			log_comment_add("		1 = disable osc+pll, set gie, halt\n");
			log_comment_add("		2 = disable pll, set gie, halt\n");
			break;
	}

	return mem_ram[addr];
}

void write_ram(u16 addr, u8 val)
{
	//TODO watch special adresses?
	switch(addr) {
		case 0:
			log_comment_add("	smb_ctl\n");
			break;
		case 1:
			log_comment_add("	smb_errno\n");
			break;
		case 2:
			log_comment_add("	i2c_errno\n");
			break;
		case 5:
			log_comment_add("	process_list MSB\n");
			break;
		case 6:
			log_comment_add("	process_list LSB\n");
			break;
		case 7:
			log_comment_add("	process_ptr MSB\n");
			break;
		case 8:
			log_comment_add("	process_ptr LSB\n");
			break;
		case 9:
			log_comment_add("	num processes = %hhd\n", val);
			break;
		case 0xa:
			log_comment_add("	halt_mode = %hhd\n", val);
			log_comment_add("		0 (or other) = set gie, don't touch anything, halt\n");
			log_comment_add("		1 = disable osc+pll, set gie, halt\n");
			log_comment_add("		2 = disable pll, set gie, halt\n");
			break;
	}

	mem_ram[addr] = val;
}

u8 read_data_flash(u16 addr)
{
	//TODO watch special adresses?
	return mem_flash[addr];
}


//TODO only FOR flash subroutine? CPU write probably not working?
void write_data_flash(u16 addr, u8 val)
{
	//TODO watch special adresses?
	mem_flash[addr] = val;
// 	fprintf(stderr, "%x %x\n",addr, mem_flash[addr]);
}


u8 read_mem(u16 addr, unsigned log)
{
	u8 ret;

	mem_r_history[trace_mem_r_idx].pc = trace_pc;
	mem_r_history[trace_mem_r_idx].addr = addr;

	trace_mem_r_idx = (trace_mem_r_idx+1)%TRACE_MEM_NUM;

	if (addr < 0x4000) {
		ret = read_ram(addr & 0x7ff);
	} else if (addr < 0x8000) {
		ret = read_data_flash(addr & 0x7ff);
	} else {
		ret = read_io(addr & 0xff);
	}

	if (log) {
		if (addr < 0x4000) {
			if (addr < 0x800) {
				log_access_read_add("%02hhx=RAM[%03hx] ", ret, addr);
			} else {
				log_access_read_add("%02hhx=RAM?[%03hx] ", ret, addr);
			}
		} else if (addr < 0x8000) {
			if (addr < 0x4800) {
				//NOTICE print phys or region offset?
				log_access_read_add("%02hhx=FLASH[%03hx] ", ret, addr & 0x7ff);
			} else {
				log_access_read_add("%02hhx=FLASH?[%03hx] ", ret, addr & 0x7ff);
			}
		} else {
			if (addr < 0x8100) {
				//NOTICE print phys or region offset?
				log_access_read_add("%02hhx=IO[%02hx] ", ret, addr & 0xff);
			} else {
				log_access_read_add("%02hhx=IO?[%02hx] ", ret, addr & 0xff);
			}
		}
	}

	return ret;
}

void write_mem(u16 addr, u8 val, unsigned log)
{
	mem_w_history[trace_mem_w_idx].pc = trace_pc;
	mem_w_history[trace_mem_w_idx].addr = addr;
	mem_w_history[trace_mem_w_idx].val = val;

	trace_mem_w_idx = (trace_mem_w_idx+1)%TRACE_MEM_NUM;

	if (addr < 0x4000) {
		write_ram(addr & 0x7ff, val);
	} else if (addr < 0x8000) {
 		write_data_flash(addr & 0x7ff, val);
		sim_breakpoint_set(SIM_BREAKPOINT_DATA);
	} else {
		write_io(addr & 0xff, val);
	}

	if (log) {
		if (addr < 0x4000) {
			if (addr < 0x800) {
				log_access_write_add("RAM[%03hx]=%02hhx ", addr, val);
			} else {
				log_access_write_add("RAM?[%03hx]=%02hhx ", addr, val);
			}
		} else if (addr < 0x8000) {
			if (addr < 0x4800) {
				//NOTICE print phys or region offset?
				log_access_write_add("readonly! FLASH[%03hx]=%02hhx ", addr & 0x7ff, val);
			} else {
				log_access_write_add("readonly! FLASH?[%04hx]=%02hhx ", addr, val);
			}
		} else {
			if (addr < 0x8100) {
				//NOTICE print phys or region offset?
				log_access_write_add("IO[%02hx]=%02hhx ", addr & 0xff, val);
			} else {
				log_access_write_add("IO?[%04hx]=%02hhx ", addr, val);
			}
		}
	}
}

void init_access(char * filename_ram, char * filename_flash)
{

	trace_pc = 0;
	trace_regs8_r_idx = 0;
	trace_regs8_w_idx = 0;
	trace_mem_r_idx = 0;
	trace_mem_w_idx = 0;

	//TODO complete RAM/IO or soft only reset? (leave tracing)
	memset(regs8_r_history, 0, TRACE_REG_NUM*sizeof(struct trace_regs8_r));
	memset(regs8_w_history, 0, TRACE_REG_NUM*sizeof(struct trace_regs8_w));
	memset(regs8_curr, 0, (UTIL_REG_FLAGS+1)*sizeof(struct trace_regs8_w));	//NOTICE last item
	memset(mem_r_history, 0, TRACE_MEM_NUM*sizeof(struct trace_mem_r));
	memset(mem_w_history, 0, TRACE_MEM_NUM*sizeof(struct trace_mem_w));

	//stack empty if set after reset (probably)
	struct cpuflags f;
	f.raw = 0;
	f.se = 1;
	write_reg8(UTIL_REG_FLAGS, f.raw, 0);

	//IRL/rom probably does "zeroing" by writing LO byte of address
	for(unsigned addr=0; addr<sizeof(mem_ram);addr++) {
		mem_ram[addr] = addr & 0xff;
	}
// 	memset(mem_ram, 0xff, sizeof(mem_ram));

	if (filename_ram) {
		FILE * fp;
		fp = fopen(filename_ram, "r");
		if (fp == NULL) {
			perror("ram dump opening");
			exit(1);
		}
		fread(mem_ram, 0x800, 1, fp);
		fclose(fp);
	}

	memset(mem_flash, 0xff, sizeof(mem_flash));

	if (filename_flash) {
		FILE * fp;
		fp = fopen(filename_flash, "r");
		if (fp == NULL) {
			perror("flash dump opening");
			exit(1);
		}
		fread(mem_flash, 0x800, 1, fp);
		fclose(fp);
	}
}
