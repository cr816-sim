#ifndef ACCESS_H
#define ACCESS_H

#include "disasm.h"

u16 read_pc(void);
void write_pc(u16 addr);
u8 read_reg8(enum alu_regs reg, unsigned log);
void write_reg8(enum alu_regs reg, u8 val, unsigned log);
void write_flags(struct cpuflags cpuflags, unsigned log);
struct cpuflags read_flags(unsigned log);
u16 read_ix(enum alu_ixs ixs, unsigned log);
void write_ix(enum alu_ixs ixs, u16 val, unsigned log);
u16 read_ip(unsigned log);
void write_ip(u16 val, unsigned log);
u8 read_ram(u16 addr);
void write_ram(u16 addr, u8 val);
u8 read_data_flash(u16 addr);
void write_data_flash(u16 addr, u8 val);
u8 read_mem(u16 addr, unsigned log);
void write_mem(u16 addr, u8 val, unsigned log);
void init_access(char * filename_ram, char * filename_flash);

#endif
