/*
 * NO WARRANTY
 * YOU ARE USING THIS SIMULATOR AT YOUR OWN RISK
 * I DON'T RECOMMEND USING THIS SIMULATOR TO CREATE ANY REAL WORLD SOFTWARE,
 * NOR USING TO DEVELOP COMMERCIAL SOFTWARES
 *
 * OTHERWISE USE IT AS OPENSOURCE (GPLv2 PREFERRED)
 *
 * ****************************************************
 *
 * compilation:
 * 		./mk.sh && ./disasm < ./MY_dump_code.bin > ./dump
 *
 * Use:
 * 		look into
 * 			sim_breakpoint_check()
 * 			do_cpu_reset() (input binary files)
 * 			disasm.h (some flags)
 * 			read rest of this comment block
 * 		no complex user input
 * 		uses fifo for command line SMB emulation
 *
 * SMB comm: look into rom_funcs.c (not 1:1 with SMB stream) or func test_buf()
 * 		(echo -e -n "\x71\x14\x02\x73\x71\x34\xfd" > ./000_smb_buf)
 * enter bootrom (unusable no rom code):
 * 		(echo -e -n "\x71\x14\x02\x73\x71\x34\xfd\x70\x17\x05" > ./000_smb_buf)
 *
 * NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE
 * you must generate event 't'/'T' (will halt-loop MCU) and then ADC/CC/timer
 * interrupt '2' and it will start doing things in halt-loop (SMB '3' too)
 * NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE
 *
 *
 *
 * TODO TODO TODO TODO TODO TODO TODO TODO TODO
 * no central control
 * rewrite trace reg RW
 * load files from command line
 * runtime flag disable reg trace (for disassemble)
 * maybe all registers right of disassm?
 * ix write from ix pre/post access? collision?
 *		cat dump | grep "[+-]" | grep "i.*i" | grep -v "ip"
 * partial static disassemble? (fcn header, footer, jump targets)
 * noninitialised access (jumpstart mode?)
 * ROM emulation assembler for better debug?
 * 		some functions are not described (inout args ABI)
 * 		original mask ROM probably impossible to readout
 * report single flag
 * add version + retbreak
 * better user input control (commands?)
 * validate opcodes with carry
 * user documented code flash calls
 * hw stack flags .. empty/full (seems to not using, but still)
 * genearte calltree
 * garden of forking paths
 * disassembler to javascript :-D
 *
 * NOTICE NOTICE NOTICE NOTICE NOTICE NOTICE
 * PFLAG(0x8090) special masking with zeroes???

Reversed variables:
[0xe2] flag level access?
	26c7:	if !xxxxxx00 -> NACK

(echo -n -e "\x71\xfd\x34" > ./000_smb_buf)
4e4b:	04af1d    move a, 0xe2               ;R{RAM[0e2]=c0 } ;W{flags=02 a=c0 pc=4e4c }
4e4c:	0ebfbf    or a, #0x40                ;R{a=c0 } ;W{flags=02 a=c0 pc=4e4d }
4e4d:	01bf1d    move 0xe2, a               ;R{a=c0 } ;W{RAM[0e2]=c0 pc=4e4e }

RAM[0e2] - access level variable

===========================================
SMB commands, safe/testing mode (use code flash, but not in operation)
needs to have set:
flash[0x60c]=0x3c
flash[0x60d]=0x7e

--- lut table <0x40, 0x4a>
(0) set addr (rom)
(1) poke byte (rom)
(2) peek byte (rom)
(3) read ramblock (rom)
328c: (4)
	SMB receive 33 bytes: (row, 32x data)
	prog data flash row
326e: (5)
	receive SMB block (LO, HI, data byte)
	prog byte into data flash FdataProgWord (rom)
	won't pre-erase (old AND new)
32aa: (6)
	receive word (row, 0), erase row of data flash
32b8: (7) mass erase if key
	(echo -e -n "\x47\xde\x83" > ./000_smb_buf)
32cb: (8)
	addition sum flash (0-0x7c0 (minus last row)), send word to SMB
266e: (9) soft reset if key
	(echo -e -n "\x49\x02\x05" > ./000_smb_buf)
	call 0x0040
		flush hw stack (8 entries, erase ram, jump vector 0)
267f: (10)
	transfer 6 fixed values to SMB host
	42, 59, 00, 84, 56, 59,
---------------------------------
lut table <0x50, 0x5a>
269a: (0)
	receive word into [byte1 0x99] [byte0 0x9a]
	[0xe2]=1
	[266] set bit 7 (process table?)
	first run enables CC
		(echo -e -n "\x50\x00\x01" > ./000_smb_buf)
		[0x99] & 1 = 0x01 enables CC calibration
		[0x99] & 1 = 0x00 don't enables CC cal
	next runs enables adc conversion
	[0x9a] is stored somewhere, test? 285b:

26a8: (1) TODO
	if ([0xe2] & 3) != 0 then NAK
	else send [0x9a]*2 words [0x269] to smb host
	simulator wont continue but IRL returns something (after a while)
	most likely some interrupts?
		cat dump | grep "0xe2, " -B 2

26b7: (2)
	receives word do [0x99] [0x9a]
	[0xe2]=2
	[266] set bit 7 (process table?)
	first run
		enables ADC (only)
	other runs
		[0x99] will be written to ADCTL0

26c5: (3) TODO? same as (1) o_O
	if ([0xe2] & 3) != 0 then NAK
	else send [0x9a]*2 words [0x269] to smb host

26d4: (4) set address (i2c or data flash)
//NOTICE there was a BUG in simulator, swapped values
	stores received word to [REG:0xe6 ADDR:0xe7] for later
	(echo -e -n "\x54\x40\x01" > ./000_smb_buf)
	i2c ADDR = 0x40
	i2c REG = 0x01
	if ADDR = 0xa0 - access data flash
		(REG + 0x4600)
	if ADDR = 0x40 - access i2c (bq29330)
		(echo -e -n "\x54\x40\x02\x59\x02\x03" > ./000_smb_buf)

26dc: (5) write byte to i2c or data flash
	use address from 0x54 command [REG:0xe6 ADDR:0xe7]
	byte 0 - value
	byte 1 - ??
	if ADDR = 0xa0 - access data flash
		(REG + 0x4600)
		the flash addresses are weirdly sorted, TODO
		NOTICE if ([0xa5] & 1) call 0x32e3 (data flash programming)
			else exit
	if ADDR = 0x40 - access i2c (bq29330)
		(echo -e -n "\x54\x40\x01\x55\x02\x03" > ./000_smb_buf)
		will write value 0x02

270a: (6) read i2c (ADDR=0x40) or data flash (ADDR=0xa0)
	REG (i2c reg or 0x4600+REG flash address)

2733: (7) set byte count (for block RW access, i2c/flash)
	CNT = [0xe5]= byte from received u16 (i2c count?)
	(echo -e -n "\x57\x12\x34" > ./000_smb_buf)
		byte count = 0x12

2741: (8) block write to i2c (ADDR=0x40) or data flash (ADDR=0xa0)
	CNT=[0xe5]
	[269]=received block from SMB
	_something_ call 0x32e3 (data flash programming) + writes bq registers

2781: (9) block read from i2c or data flash
	ADDR, REG, CNT

27bc: (10) ???
	read out RAM[0x98]
		send as u16
		26dc:?? (perhaps more)

72:
	27f7: const 1111
73:
	sends prog vector 6 (for key)
80:
	receive word
	==0 writes default values (dsg/chg off) to i2c reg1 + readback
	!=0 enables timer

===========================================
normal mode
which are SBS?
0x3c - returns 0x13 bytes, part const, part from ram, ram differs on HW
	can read/write
0x3d - RW word
	test if \x0f\x51 (and other sequences)
0x3e - RW word
	test bitfields
0x3f - tests if equals last write, if not restarts SMB, if yes ??? TODO
	real HW wont let force new value (0xa2 0x38 or 0x2c 0x38)

0x40 SetAddr() (rom)
0x41 PokeByte() (rom)
	only if (RAM[0e2] & 0x40)	== must be unlocked with code@6 password (4e4b)
		(echo -e -n "\x71\x14\x02\x73\x71\x34\xfd" > ./000_smb_buf)
0x42 peekbyte() (rom)
	i2ctransfer -y 0 "w1@0xb" 0x40 "w2@0xb" 0x00 0x40 ; i2ctransfer -y 0 "w1@0xb" 0x42 "r2@0xb"
		start of flash
0x43 ReadRAMBlk() (rom)
0x44 TODO
	needs unlock
	unknown function, probably writes dynamic flash config area at 4600
	receives N bytes (TODO maybe non divisible by 32), can be more, like 64 etc..
	TEST read 32B -> erase (2 rows), write first with new data, write second with backuped values
	FdataEraseRow
	FdataProgRow
	possible offset? RAM[004] (seems to used only in init)
0x45 TODO
	needs unlock
	sends 14 bytes of data, originating from flash (opposite of 0x44)

0x50 	returns constant bytes (similar as 4a in safe mode)
	if RAM[0e2] & 0x40
			3 bytes: 42, 59, 00
	else
			4 bytes: 42, 59, 00, 84
0x51 returns 10 bytes from RAM[0ff] (address + len is permanent)
0x52 returns bytes from start of RAM (TODO)
	53,54 similar
	dumps from real HW are different

72:
	4e6d: sends const 0000
70:
	needs unlock
	expects 2 bytes (word)
	\x70\x02\x05 reboot
		(echo -e -n "\x71\x14\x02\x73\x71\x34\xfd\x70\x02\x05" > ./000_smb_buf)
	\x70\x17\x05 enter bootrom
73:
	part of unlock sequence, sends word key from code space @0x0006
	FlashRdRow() (rom)
3f:
	can read or write word originating from flash[610]
3e:
	read/write word from RAM[29b]
	unknown use
	init to 1, 0
	blocked acupack shows no change
3d:
	R/W word from RAM[0e9]
	0,0
3c:
	R/W "ibm corporation" + 4B
		4B created by some operations with RAM[0f0], RAM[0ec], RAM[0eb]
3b:
	sends word from RAM[29d]
	real hw differs
30:
	sends block of 10 bytes from RAM[02d]
	originates from FLASH[616]
31:
	sends block of 16 bytes
	computed from FLASH[480] and higher
32:
	sends block of 16 bytes
	computed from FLASH[4c0] and higher
33:
	sends block of 24 bytes
	zeroes 24 bytes in buffer o_O
2f:
	sends battery serial value from length:FLASH[7a0], data:FLASH[7a1]
23: sends block of computed data, some variables are from static flash section
	should be cell voltages
22: sends "lion" string from flash
21: sends part number string
20: sends manuf string

0: manuf access, not enabled by default ? enabled by FLASH[080]

===========

46a2	call na i2c write
	R3=adresa
	R2=reg
	R1=count
	stack[0]=pointer to data
	references from:
		0d1b vvv
		26dc jumped from 2922 (lut 5), dynamic trampoline? (call 27f2)
			26e8 cannot jump (smb error)
			26ec must jump ([0xe6]!=0xa0)

		2741 ??? (+ receive block from smb, write to bq), jumped from 2925 (8) (call 27f2)
			2756 cannot jump, must be [0xe7]==0x40 (adresa bq)
			2759 CNT, anything always bigger than 0? (cannot be 0)

0cce: wait until masked BQ reg has two successive reads same
	R3 = cislo registru
	r2 = store ptr MSB read
	r1 = store ptr LSB read
	r0 = compare mask?
0d1b: write reg to BQ, readback and compare masked values
	r3 = cmd
	r2 = val
	r1 = mask

4a7a:
	lots of SMB calls
4cbd:
	"ibm corp"
0ca0:
	[0x66] ??

 todo
 5618:	04bff7    or a, 0x08                    ;R: a=02 RAM[008]=5a     ;W: flags=02 a=5a a=5a pc=5619
double

??
005d:	058ea6    cpl1 r0, 0x59                 ;R: r0=00 RAM[059]=00     ;W: flags=00 a=ff r0=ff pc=005e
005e:	058da7    cpl1 r1, 0x58                 ;R: r1=b8 RAM[058]=00     ;W: flags=00 a=ff r1=ff pc=005f

0014:	0e1cfe    cmp r2, #0x01                 ;R: r2=00     ;W: flags=03 a=01 pc=0015
jumps to 0029:   6c b7 3a        calls 0x4893

0075:   ff 15 0e        cmp i2h, #0x00
0076:   87 ff 32        jzc 0x0078      ;zero clear, !=
0077:   00 14 0e        cmp i2l, #0xff
0078:   7c ff 34        jcs 0x0083      ;carry set, <=

282f:
	XOR discharge bit
2b1a:
	set bits?, [0x66]
	call @2bd4

283b:
49c3: TODO

trampoline 0x8009 for 2912/291d
	27c4 entry
	[8013/data ready] vvv (else NAK)
	[smb data] load data
	27df: 0x40 = command in R3 <0x40, 0x4a> (table 2912)
	27ea: 0x50 = command in R3 <0x50, 0x5a> (table 291d)


!!!!!! 2bb7 setup bitu 1
	2bb4 must jump
		[0x9f] musi byt
			x1xx xx1x
			x0xx xx1x
			x0xx xx0x
		2bb7: important test?
			[0x6b]	xxxx xx0x
		[0x66] & 0x1e cannot equal i2h (cache?)

		head?? 2b1a:
			called from 5119:
				510e
				head? 5090:
				indirect jump? 493b:


 *
 * 283b xor dsg
 * 2bd4 maybe set here ? or i2h, r3, i2h
 * 0c99
 *
 * 0x47aa call delay

0x468f bq write R2=reg1
	0d48

0x0cab	want to write bq reg1

0xcf4 call to 0x468f
0xcff

0cf5:	register jump table?

how to get there?
2bd7:move 0x66, i2h
	intro? 2b1a


write_data_flash(0x82, 0xf7);
	writes to IO[0xb0] pflag?, IO[93]? wakeup?
		if ((([0x82]*8)>>8)&1) -> ^^^
			ekvivalent (& 0x20)

enable adc conversion register
cat dump | grep 'move i0h, #0x80' -A 1 -B 1 | grep 'move i0l, #0x30' -A 3
0610: b9	entry@05f7
	call@096c,entry@093c
		call@361d, entry@3611
			possible indirect call set @ 4996, entry@493b??
				call@002e
	call@0a30, entry@0976?
		call@3623, entry@3611 ^^^
	call@0aea,entry@0a8c
		call@3625, entry@3611 ^^^
	call@0baf, entry@0bad
		call@36a8, entry@3696
			call@3687, entry@3611 ^^^
	call@0c3a,entry@0bb5
		call@35f8, entry@3566
			call@3615, entry@3611 ^^^
063c: be	entry@05f7
0641: b9	entry@05f7
06bc: b9	entry@05f7
06e4: b9	entry@05f7
06f2:
09cf:
09d4:
289e:
28e2:

load i2c/flash value into ram and do weird operations
0167:

stuck in halt
4a34: leave test?

adc@8030 possible IRL sequence
00 9d 1d c1 8d 00
TMRCTLW@8060
0x04
timers disabled (but not discharging)
ra_out 0x54
rc_out sequence 0x5f (long) 0x5b (short)
RA_IN 0x44
RB_IN 0x00
RC_IN 0x0c, short 0x00
PIE 0xb8, timer enabled
VTRIM 0x21
SMBCTL !!! (simulator isolate=1, IRL=0x00)


process table @ RAM[25a]
0: u16	new process stack pointer
2: u8	 status?
		& 0x20 jump to irq return
		& 0x40 don't set gie (maybe already set), if ==0 set stack from pointer?
		|= 0x8 before call 0x4a2a
		& [+7] & [+0xc]
		& 0x80 == 0 .. test if event .. else set this bit

RAM[25a] i3h
RAM[25b] i3l
	i3 = 76c (boot constant), 73f (last set before main loop)
RAM[25c] bitfield stat related (in looping)
RAM[25d], RAM[25e] - unused

RAM[25f] RAM[260] i3
	6ec - boot, 6e4 - last set, many uses then
RAM[261] bitfield mask stat related (in looping)
	cat z01_vanilla.log | grep 'RAM\[261\]' -A 0 -B 2
RAM[262],RAM[262] unused

RAM[264] i3h
RAM[265] i3l
RAM[266] bitfield, stat related (in looping)

bitfields = maybe irq/event handlers?

NOTICE NOTICE NOTICE NOTICE
search for i2cwrite cals!
probably automate
NOTICE only closest entry was found (theoretically can be jumped anywhere from entire prog space + dynamically)
"call 0x803c i2cwrite" @ 4740, entry @ 46da (R2 = i2c reg)
	call @ 469c, entry @ 468f
		call @ 0cf4, entry @ 0cce
		call @ 0cff, entry @ 0cce
		call @ 0d48, entry @ 0d1b
		call @ 2726, entry @ 270a
		call @ 2799, entry @ 2781
	call @ 46af, entry @ 46a2
		call @ 0d3d, entry @ 0d1b
		call @ 2702, entry @ 26dc (contains smbSlaveRcvWord)
		call @ 2766, entry @ 2741 (contains smbSlaveRcvBlock)
	call @ 46c2, entry @ 46b5
		safemode SMB 0x40 trampoline entry
	call @ 46d4, entry @ 46c8
		no direct call


flash adresy dat
600/640/6c0
500

0x486c - memcpy

*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <string.h>
#include "disasm.h"
#include "log.h"
#include "rom_funcs.h"
#include "access.h"
#include "isa.h"
#include "ioregs.h"


/** ****** tracers utils ******/

static enum interrupt_event do_event = INT_NONE;

static unsigned sim_breakpoint_flags = 0;

void sim_breakpoint_set(unsigned flags)
{
	sim_breakpoint_flags |= flags;
}

/** ******** simulator *******/

extern void write_bq29330_reg(u8 reg, u8 val);

static void do_cpu_reset(void)
{
// 	sim_breakpoint_set(SIM_BREAKPOINT_STEP);

	char * filename_ram = NULL;
	char * filename_eeprom = NULL;
	char * filename_prog = NULL;
	char * filename_iospace = NULL;

// 	filename_ram = "./DUMP_ram.bin";
	filename_eeprom = "./DUMP_eeprom.bin";
	filename_prog = "./DUMP_code.bin";
// 	filename_iospace = "./DUMP_io.bin";

	init_access(filename_ram, filename_eeprom);
	init_isa(filename_prog);
	init_ioregs(filename_iospace);

	write_bq29330_reg(0, 0x18);
	write_bq29330_reg(1, 0xff);
	write_bq29330_reg(2, 0);
	write_bq29330_reg(3, 0);
	write_bq29330_reg(4, 0);
	write_bq29330_reg(5, 0);
	write_bq29330_reg(6, 0);
	write_bq29330_reg(7, 0);
	write_bq29330_reg(8, 0);

#if 0
	//bit 2 can enable smb call 0 write (manuf access)
	//original = 0x30
	write_data_flash(0x80, 0x34);
#endif

#if 0
	//erase parts of flash
	for (addr = 0x580; addr < 0x600; addr++) {
		write_data_flash(addr, 0xff);
	}
#endif

//HACK ing
//bit0 = 0
// mem_flash[0x083] &= ~1;
// mem_flash[0x083] |= 1;
// mem_flash[0x083] |= 0xff;

#if 0
	//enters safemod with this
	for (addr = 0x60c; addr < 0x60e; addr++) {
		write_data_flash(addr, 0xff);
	}
#endif

#if 0
	//default bq registers?
	write_data_flash(0x601, 0xff);
#endif

#if 0
	//maybe bad description ???call function with discharge XOR bit, bud the call never returns
	write_data_flash(0x60c,  0x3c);	//pc=4926
	write_data_flash(0x60d, 0x7e);	//4928
	write_data_flash(0x83, 0x12);	//0c50
#endif

#if 0
	//enter safe mode?
	//ability to direct i2c write
	write_data_flash(0x60c,  0x3c);	//pc=4926
	write_data_flash(0x60d, 0x7e);	//4928
//unused	write_data_flash(0x83, 0x12);	//0c50
#endif

#if 0
	//manuf access? will enable word receive from host (instead transmit only) - still it does nothing special?
	//(echo -e -n "\x00\xff\x00\x20" > ./000_smb_buf)
	write_data_flash(0x80, 0x34);
#endif

// 	write_data_flash(0x80, 0x34);
//
// 	write_data_flash(0x58, 0xff);
// 	write_data_flash(0x59, 0xff);

#if 0
//system config words (gpios ...)
// write_data_flash(0x80, 0xf7);
// write_data_flash(0x82, 0xf7);
// write_data_flash(0x83, 0xf7);
#endif

// write_data_flash(0x540, 0x30);
// write_data_flash(0x541, 0xcf);

// write_data_flash(0x700, 0xff);
// write_data_flash(0x701, 0xff);
// write_data_flash(0x702, 0xff);
// write_data_flash(0x703, 0xff);

#if 1
//maybe correct watchdog startup?
write_io_lowlevel(0x60, 4 | 8);
#endif

#if 0
//uninitialized read (test if unpowered reboot?)
write_ram(0x58, 0xff);
write_ram(0x59, 0xff);
#endif

#if 0
//uninitialized read2 (test if unpowered reboot?)
//compared with checksum? compared with flash checksum too 0x4600
write_ram(0x57, 0x0);

write_ram(0x58, 0xa6);
write_ram(0x59, 0x70);
#endif


// write_io_lowlevel(0x30, 0x9d);
// write_io_lowlevel(0x91, 0xb8);

// for (unsigned addr=0x500;addr<0x520;addr++) {
// 	write_data_flash(addr+0x100, read_data_flash(addr));
// }


// write_data_flash(0x60c, 0x30);
// write_data_flash(0x60d, 0xcf);

#if 0
//safe mode?
// write_data_flash(0x60c, 0x3c);
// write_data_flash(0x60d, 0x7e);
#endif

// write_data_flash(0x60c, 0x5a);
//write_data_flash(0x60d, 0x1f);


// sim_breakpoint_set(SIM_BREAKPOINT_STEP);
}


#define SMB_DATA_MAX 100
static u8 smb_data_val[SMB_DATA_MAX+1];
static unsigned smb_data_len=0;
static unsigned myoff=0;

u8 * test_buf(void)
{
	u8 * ptr = &smb_data_val[myoff];

	myoff++;
	if(myoff>=smb_data_len) {
		myoff=0;
	}
	return ptr;
}


// unsigned xxxxxx=0;

static void sim_breakpoint_check(u16 pc)
{
#if 0
	switch(pc) {
		case 0x0000:
			sim_breakpoint_set(SIM_BREAKPOINT_CODE);

			break;
	}
#endif

	if (sim_breakpoint_flags == 0)
		return;

	fprintf(stderr, "Execution paused (%x): " , sim_breakpoint_flags);

	if (sim_breakpoint_flags & SIM_BREAKPOINT_CODE) {
		fprintf(stderr, "PC address breakpoint: %04hx, ", pc);
	}

	if (sim_breakpoint_flags & SIM_BREAKPOINT_HALT) {
		fprintf(stderr, "Processor halted, ");
	}

	unsigned first_step = 0;
	if (sim_breakpoint_flags & SIM_BREAKPOINT_STEP) {
		//spam?
		fprintf(stderr, "Stepping, ");
		first_step = 1;
	}

	if (sim_breakpoint_flags & SIM_BREAKPOINT_DATA) {
		fprintf(stderr, "Data breakpoint, ");
	}

	fprintf(stderr, "\n");

	static u32 adc_val = 10;
	static s16 cc_val = -10;
	int fd;

	//TODO TODO TODO better continue, (only enter/space?), readline!
	while ((sim_breakpoint_flags & (~SIM_BREAKPOINT_STEP)) ||
		first_step) {

		first_step = 0;

		int val;
		fprintf(stderr, "> ");
		system("/bin/stty raw");
		val = getchar();
		system("/bin/stty cooked");

		sim_breakpoint_flags &= SIM_BREAKPOINT_STEP;

		switch (val) {
 			case 't':
				do_event = INT_EV0;
				//NOTICE seems to end in infinite loop
 				break;
 			case 'T':
				do_event = INT_EV1;
				//NOTICE seems to end in infinite loop
 				break;
			case '0':
				do_event = INT_V0_RESET;
				break;
			case '1':
				do_event = INT_V1_IN1;
				break;
			case '2':	//ADC/coloumb/timer interrupt
				adc_val = 907874;
				write_io_lowlevel(0x32, (adc_val >> 16) & 0xff);
				write_io_lowlevel(0x33, (adc_val >> 8) & 0xff);
				write_io_lowlevel(0x34, (adc_val >> 0) & 0xff);
				cc_val--;
				write_io_lowlevel(0x41, (cc_val >> 8) & 0xff);
				write_io_lowlevel(0x42, (cc_val >> 0) & 0xff);

//ADC_DRDY
write_io_lowlevel(0x31, read_io_lowlevel(0x31) | 1);


				//ADF
   				write_io_lowlevel(0x90, read_io_lowlevel(0x90) | 0x20);
				//CCF
// 				write_io_lowlevel(0x90, read_io_lowlevel(0x90) | 0x10);
				//TIMF
//   				write_io_lowlevel(0x90, read_io_lowlevel(0x90) | 0x8);
				//WKF
 				write_io_lowlevel(0x90, read_io_lowlevel(0x90) | 0x4);
				//TMAF
// 				write_io_lowlevel(0x90, read_io_lowlevel(0x90) | 0x2);
				//TMBF
// 				write_io_lowlevel(0x90, read_io_lowlevel(0x90) | 0x1);

// fprintf(stderr, "dfgdfgd %x %x\n", write_io_lowlevel(0x90], write_io_lowlevel(0x91]);
				//if enabled at least one
				if ((read_io_lowlevel(0x90) & read_io_lowlevel(0x91)) & 0x3f) {
					do_event = INT_V2_IN2;
				}
				break;
			case '3':	//SMB interrupt (DRDY)
log_comment_add("=== Set IRQ values ===");
				write_io_lowlevel(0x13, 0x24);

// 				smb_data_val = 0x0;
				write_io_lowlevel(0x11, test_buf()[0]);

				//SMBF
				write_io_lowlevel(0x90, read_io_lowlevel(0x90) | 0x80);
				//HDQF
				//write_io_lowlevel(0x90] |= 0x40;

				//if enabled at least one
				if ((read_io_lowlevel(0x90) & read_io_lowlevel(0x91)) & 0xc0) {
					do_event = INT_V3_IN0;
				}
log_comment_add("=== Run ===");

				break;
			case '4':
				do_event = INT_V4_SMB_WAIT;
				break;
/*
 * NOTICE
 * vector 5: image signature, other values will stay in bootrom
 * vector 6: security word (0x3fffff = undefined)
 */
			case '7':
				do_event = INT_V7_XIN;
				break;
			case '8':
				do_event = INT_V8_CIN;
				break;
			case '9':
				do_event = INT_V9_CLKHI;
				break;
			case 'a':
				do_event = INT_VA_CLKLO;
				break;
			case 'b':
				do_event = INT_VB_WAIT_CLKHI;
				break;
			case 'c':
				do_event = INT_VC_DATAHI;
				break;
			case 'd':
				do_event = INT_VD_DATALO;
				break;
			case 'e':
				do_event = INT_VE_DATAIN;
				break;
			case 'f':
				do_event = INT_VF_WAIT;
				break;
			case 'R':	//dump registers
				fprintf(stderr, "Dump regs:\n");

				for (unsigned reg=0;reg<16;reg++) {
					fprintf(stderr, "%s=%02hhx ",
						   get_regs_name(reg),
						   read_reg8(reg, 0)
					);
				}
				fprintf(stderr, "%s=%02hhx\n",
						get_regs_name(UTIL_REG_FLAGS),
						read_reg8(UTIL_REG_FLAGS, 0)
				);

				//don't continue in exec
				sim_breakpoint_set(SIM_BREAKPOINT_STDIN);
				break;
			case 'I':	//dump IO
				log_buf("Dump IO", 0x8000, 0x100);

				//don't continue in exec
				sim_breakpoint_set(SIM_BREAKPOINT_STDIN);
				break;
			case 'q':
				fprintf(stderr, "Quitting\n\n");
				exit(0);
				break;
			case 's':
				sim_breakpoint_flags |= SIM_BREAKPOINT_STEP;
				break;
			case 'S':
				sim_breakpoint_flags &= ~SIM_BREAKPOINT_STEP;
				break;
			case 'm':
				log_buf("Dump Flash", 0x4000, 0x800);

				//don't continue in exec
				sim_breakpoint_set(SIM_BREAKPOINT_STDIN);
				break;
			case 'M':
				log_buf("Dump RAM", 0x000, 0x800);

				//don't continue in exec
				sim_breakpoint_set(SIM_BREAKPOINT_STDIN);
				break;
			case 'x':	//SMB test extern

	fd = open("./000_smb_buf", O_RDONLY /*| O_NONBLOCK*/);
fprintf(stderr, "\nfd %i\n",fd);
	if (fd != -1) {
		smb_data_len = read(fd, smb_data_val, SMB_DATA_MAX);
		close(fd);
myoff=0;
		fprintf(stderr, "%02hhx\n", smb_data_val[0]);
		fprintf(stderr, "%02hhx\n", smb_data_val[1]);
		fprintf(stderr, "%02hhx\n", smb_data_val[2]);
		fprintf(stderr, "%02hhx\n", smb_data_val[3]);

		fprintf(stderr, "l:%i errno:%i\n",
			smb_data_len,
			errno
			);
	} else {
		fprintf(stderr, "\n!open error %i\n", errno);
	}
				//don't continue in exec
				sim_breakpoint_set(SIM_BREAKPOINT_STDIN);

				break;
			default:
				break;
		}
	}
	fprintf(stderr, "\n");

	//don't erase only stepping
	sim_breakpoint_flags &= SIM_BREAKPOINT_STEP;
}


////////////////////////////
/////////   MAIN   /////////
////////////////////////////

//  unsigned xxxcount=0;

int main(int argc, char *argv[])
{
	u16 curr_pc;
	struct reg_stat stat;
	struct opcode_word opcode;

	do_cpu_reset();

	while (1) {
		stat.raw = read_reg8(MAIN_REG_STAT, 0);

// xxxcount++;
// if (xxxcount > 3000) {
// 	sim_breakpoint_set(SIM_BREAKPOINT_CODE);
// 	xxxcount=0;
// }

		curr_pc = read_pc();

		switch(do_event) {
			case INT_EV0:
				do_event = INT_NONE;
 				stat.ev0 = 1;
				write_reg8(MAIN_REG_STAT, stat.raw, 0);
				break;
			case INT_EV1:
				do_event = INT_NONE;
 				stat.ev1 = 1;
				write_reg8(MAIN_REG_STAT, stat.raw, 0);
				break;
			case INT_V0_RESET:
				do_event = INT_NONE;
				stat.gie = 0;
				stat.ie1 = 0;
				stat.ie2 = 0;
				write_reg8(MAIN_REG_STAT, stat.raw, 0);

				write_pc(0);
				//init stack
				//clear all regs
				//modes disabeld
				//do_cpu_reset(); ??
				break;
			case INT_V1_IN1:
				do_event = INT_NONE;
				stat.in1 = 1;

// stat.ie1 = 1;

				if (stat.gie && stat.ie1) {
					stat.gie = 0;

// 					mem_data[0x8090] |= 0xff;

					//~ call jump (but return to this instruction, not next)
					do_push(curr_pc);
					write_pc(1);
				}
				write_reg8(MAIN_REG_STAT, stat.raw, 0);
				break;
			case INT_V2_IN2:	//adc cc timer
				do_event = INT_NONE;
				stat.in2 = 1;
				if (stat.gie && stat.ie2) {
					stat.gie = 0;
					//return to this
					do_push(curr_pc);
					write_pc(2);
				}
				write_reg8(MAIN_REG_STAT, stat.raw, 0);
				break;
			case INT_V3_IN0:	//smb
				do_event = INT_NONE;
				stat.in0 = 1;

				if (stat.gie) {
					stat.gie = 0;
					do_push(curr_pc);
					write_pc(3);
				}
				write_reg8(MAIN_REG_STAT, stat.raw, 0);
				break;
			//TODO are these GIE dependant or just sw?
			case INT_V4_SMB_WAIT:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(4);
				break;
			case INT_V7_XIN:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(7);
				break;
			case INT_V8_CIN:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(8);
				break;
			case INT_V9_CLKHI:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(9);
				break;
			case INT_VA_CLKLO:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(0xa);
				break;
			case INT_VB_WAIT_CLKHI:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(0xb);
				break;
			case INT_VC_DATAHI:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(0xc);
				break;
			case INT_VD_DATALO:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(0xd);
				break;
			case INT_VE_DATAIN:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(0xe);
				break;
			case INT_VF_WAIT:
				do_event = INT_NONE;
				do_push(curr_pc);
				write_pc(0xf);
				break;
			case INT_NONE:
			default:
				//normal run
				break;
		}

		//load new instruction (can be irq vector jump)
		curr_pc = read_pc();
		opcode = read_code(curr_pc);

		log_addr(curr_pc);
		log_opcode(opcode);

		curr_pc = opcode_decode(opcode);

#ifndef JUST_DISASSEMBLE
		curr_pc = check_rom_funcs(curr_pc);
#endif

#ifdef JUST_DISASSEMBLE
		curr_pc = read_pc()+1;
		//TODO use code dump filesize
		if (read_pc() == 0x5680)	//trailing NOPs
// 		if (read_pc() == 0xffff)	//not overflow
			return 0;
#endif

		write_pc(curr_pc);

#ifdef JUST_DISASSEMBLE
		log_flush(1);
#else
		log_flush(0);
#endif


// 		printf("XXXXXXXXX %i\n",sim_breakpoint_flags);

#ifdef JUST_DISASSEMBLE
		sim_breakpoint_flags = 0;
#endif

		sim_breakpoint_check(curr_pc);
	}

	return 0;
}
