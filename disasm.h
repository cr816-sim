#ifndef DISASM_H
#define DISASM_H
#include <stdint.h>
#include <stdio.h>

// #define JUST_DISASSEMBLE
#define QUIRK_TESTB_IS_AND
#define STACK_LEN		8
#define MEM_PROG_LEN	0x10000

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

typedef uint64_t u64;

//TODO move elsewhere!
#define SIM_BREAKPOINT_CODE		(1<<0)
#define SIM_BREAKPOINT_HALT		(1<<1)
#define SIM_BREAKPOINT_STEP		(1<<2)
#define SIM_BREAKPOINT_DATA		(1<<3)
#define SIM_BREAKPOINT_STDIN	(1<<4)


//events
enum interrupt_event {
	INT_NONE = 0,
	//coolrisc software events
	INT_EV0,
	INT_EV1,
	//coolrisc standard events
	INT_V0_RESET,
	INT_V1_IN1,	//io probably
	INT_V2_IN2,	//adc/coloumb/timer
	INT_V3_IN0,	//SMB bus
	//??
	INT_V4_SMB_WAIT,
	//redirect
	INT_V7_XIN,
	INT_V8_CIN,
	//i2c user
	INT_V9_CLKHI,
	INT_VA_CLKLO,
	INT_VB_WAIT_CLKHI,
	INT_VC_DATAHI,
	INT_VD_DATALO,
	INT_VE_DATAIN,
	INT_VF_WAIT,

	INT_SIM_NOOP = 0xff,
};


enum jmp_type {
	JMP_CC = 0,
	JMP_VC = 1,
	JMP_ZC = 2,
	JMP_UNCOND = 3,
	JMP_CS = 4,
	JMP_VS = 5,
	JMP_ZS = 6,
	JMP_EVT = 7,
};

enum jmp_class {IP, IMM};


enum alu_regs {
	MAIN_REG_I0L = 0,
	MAIN_REG_I0H = 1,
	MAIN_REG_I1L = 2,
	MAIN_REG_I1H = 3,
	MAIN_REG_I2L = 4,
	MAIN_REG_I2H = 5,
	MAIN_REG_I3L = 6,
	MAIN_REG_I3H = 7,
	MAIN_REG_IPL = 8,
	MAIN_REG_IPH = 9,
	MAIN_REG_STAT = 0xa,
	MAIN_REG_R3 = 0xb,
	MAIN_REG_R2 = 0xc,
	MAIN_REG_R1 = 0xd,
	MAIN_REG_R0 = 0xe,
	MAIN_REG_A = 0xf,
};
#define UTIL_REG_FLAGS (0x10)


enum alu_op4 {
	OP4_CMPA = 0,
	OP4_CMP = 1,
	OP4_AND = 2,
	OP4_SUBS = 3,
	OP4_SUBD = 4,
	OP4_SUBDC = 5,
	OP4_MULA = 6,
	OP4_SUBSC = 7,
	OP4_XOR = 8,
	OP4_UNKNOWN_09 = 9,
	OP4_MOVE = 0xa,
	OP4_OR = 0xb,
	OP4_ADD = 0xc,
	OP4_ADDC = 0xd,
	OP4_MUL = 0xe,
	OP4_TESTB = 0xf,
};


enum alu_op5 {
	OP5_CMPA = 0,
	OP5_CMP = 1,
	OP5_AND = 2,
	OP5_SUBS = 3,
	OP5_SUBD = 4,
	OP5_SUBDC = 5,
	OP5_MULA = 6,
	OP5_SUBSC = 7,
	OP5_XOR = 8,
	OP5_UNKNOWN_09 = 9,
	OP5_MOVE = 0xa,
	OP5_OR = 0xb,
	OP5_ADD = 0xc,
	OP5_ADDC = 0xd,
	OP5_MUL = 0xe,
	OP5_UNKNOWN_0F = 0xf,
	OP5_SHRA = 0x10,
	OP5_INC = 0x11,
	OP5_CMVD = 0x12,
	OP5_CMVS = 0x13,
	OP5_SHRC = 0x14,
	OP5_INCC = 0x15,
	OP5_SHR = 0x16,
	OP5_UNKNOWN_17 = 0x17,
	OP5_CPL1 = 0x18,
	OP5_CPL2 = 0x19,
	OP5_SHL = 0x1a,
	OP5_DEC = 0x1b,
	OP5_CPL2C = 0x1c,
	OP5_UNKNOWN_1D = 0x1d,
	OP5_SHLC = 0x1e,
	OP5_DECC = 0x1f,
};


enum alu_ixs {
	I_REG_0 = 0,
	I_REG_1 = 1,
	I_REG_2 = 2,
	I_REG_3 = 3,
};

//trace

struct trace_mem_r {
	u16 pc;
	u16 addr;
};

struct trace_mem_w {
	u16 pc;
	u16 addr;
	u8 val;
};

struct reg_stat {
	union {
		struct {
			//LSb
			u8 ev0:1;	//wev (internal wake timer)
			u8 ev1:1;	//xev (external timer?)
			u8 in0:1;	//cin (smb)
			u8 in1:1;	//xin (int pin)
			u8 in2:1;	//pin (adc/cc/tmr)
			u8 gie:1;	//gie (global interrupt, pin+xin+cin)
			u8 ie1:1;	//xine (enable xin)
			u8 ie2:1;	//or pine (enable pin)
			//MSb
		};
	u8 raw;
	};
};

struct cpuflags {
	union {
		struct {
			//LSb
			u8 v:1;
			u8 c:1;
			u8 z:1;
			u8 sf:1;	//stack full
			u8 se:1;	//stack empty
			//MSb
		};
		u8 raw;
	};
};

struct trace_regs8_r {
	u16 pc;
	enum alu_regs reg;
};

struct trace_regs8_w {
	u16 pc;
	enum alu_regs reg;
	union {
		//same order as enum alu_regs (which doesn't matter here :-P)
		u8 i0l;
		u8 i0h;
		u8 i1l;
		u8 i1h;
		u8 i2l;
		u8 i2h;
		u8 i3l;
		u8 i3h;

		u8 ipl;
		u8 iph;

		//stat register (irq one)
		struct reg_stat stat;

		u8 r3;
		u8 r2;
		u8 r1;
		u8 r0;

		u8 a;

		//additional fields (separate)
		struct cpuflags flags;

		//raw access for u8 variables/filling
		u8 raw;
	};
};



//zasobnik pro kazdou z polozek (reg, mem, flag...)
//+ PC na kazdy zaznam

struct opcode_word {
	union {
		struct {
			u32 offset:8;
			u32 dst:4;
			u32 alu_op:5;
			u32 ix:2;
		} alu1;
		struct {
			u32 cpl2_offset:7;
			u32 sign:1;
			u32 dst:4;
			u32 alu_op:5;
			u32 ix:2;
		} alu2;
		struct {
			u32 n_data:8;
			u32 dst:4;
			u32 alu_op:4;
		} alu3;
		struct {
			u32 dst:4;
			u32 op1:4;
			u32 op2:4;
			u32 alu_op:5;
		} alu4;
		struct {
			u32 ix:2;
			u32 code1:6;
			u32 dst:4;
			u32 alu_op:5;
		} alu5;
		struct {
			u32 n_addr:8;
			u32 dst:4;
			u32 alu_op:5;
		} alu6;
		struct {
			u32 code1:8;
			u32 src:4;
			u32 ix:2;
		} move7;
		struct {
			u32 cpl2_offset:7;
			u32 sign:1;
			u32 src:4;
			u32 ix:2;
		} move8;
		struct {
			u32 offset:8;
			u32 src:4;
			u32 ix:2;
		} move9;
		struct {
			u32 n_addr:8;
			u32 src:4;
		} move10;
		struct {
			u32 n_addr:8;
			u32 n_data:8;
		} move11;

		struct {
			u32 n_addr:16;
		} calls_imm;
		struct {
			u32 n_addr:16;
		} call_imm;
		struct {
			u32 n_addr:16;
			u32 cc:3;
		} jmp_imm;

		struct {
			u32 code1:8;
			u32 on:1;
		} pmd;

		struct {
			u32 div:4;
		} freq;


		u32 raw;
	};
};

struct opcode_decoder {
		u32 and_mask;
		u32 equal;
		u16 (*opcode_fcn)(struct opcode_word);
};

void sim_breakpoint_set(unsigned flags);


#endif
