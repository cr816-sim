#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "disasm.h"
#include "log.h"

static u8 io_space[0x100];

static const char *name_smbsta_bits[8] = {
	"master", "unit_busy", "bus_free", "nacked", "sa_rdy", "d_rdy", "d_req", "bus_low",
};

static const char *name_smbctl_bits[8] = {
	"sa_en", "isolate", "smb_rst", "bfi_en", "pec_dis", "bli_dis", "bli_edge", "nc_smb",
};

static const char *name_smbpec_bits[8] = {
	"pec_vld", "pec_chk", "pec_snd", "rsvd", "rsvd", "rsvd", "rsvd", "rsvd",
};

static const char *name_hdqsta_bits[8] = {
	"data_in", "sbit_in", "hdq_drdy", "hdq_drq", "rsvd", "rsvd", "rsvd", "rsvd",
};


static const char *name_hdqout_bits[8] = {
	"data_out", "sbit_out", "rsvd", "rsvd", "rsvd", "rsvd", "rsvd", "rsvd",
};

static const char *name_adctl0_bits[8] = {
	"chan0", "chan1", "chan2", "chan3", "fast", "adc_on", "vrvdd", "conv",
};

static const char *name_ccctl_bits[8] = {
	"cc_cal", "cc_on", "rsvd", "rsvd", "rsvd", "rsvd", "rsvd", "rsvd",
};

static const char *name_clk_bits[8] = {
	"pll_en", "osc_en", "osc_ng", "rsvd", "dg0", "dg1", "rsvd", "rsvd",
};

static const char *name_tmrctlw_bits[8] = {
	"wdcrst", "wken", "wden", "wdf", "rsvd", "rsvd", "rsvd", "rsvd",
};

static const char *name_tmrctla_bits[8] = {
	"tma_ps0", "tma_ps1", "tma_edg", "tma_isrc", "tma_ext", "tma_on", "tm16", "rsvd",
};

static const char *name_tmrctlb_bits[8] = {
	"tmb_ps0", "tmb_ps1", "tmb_edg", "tmb_isrc", "tmb_ext", "tmb_on", "pwb0", "pwb1",
};

static const char *name_tmrctlc_bits[8] = {
	"cap_ps0", "cap_ps1", "cap_edg", "mode0", "mode1", "mode2", "pwa0", "pwa1",
};

static const char *name_raout_bits[8] = {
	"rao0", "rao1/vo", "rao2", "rao3", "rao4", "rao5", "rao6", "rao7",
};

static const char *name_rbout_bits[8] = {
	"rbo0", "rbo1", "rbo2", "rbo3", "rbo4", "rbo5", "rbo6", "rbo7",
};

static const char *name_rcout_bits[8] = {
	"rco0", "rco1", "rco2", "rco3", "rco4", "rco5", "rco6", "rco7",
};

static const char *name_rain_bits[8] = {
	"rai0", "rai1", "rai2", "rai3", "rai4", "rai5", "rai6", "rai7",
};

static const char *name_rbin_bits[8] = {
	"rbi0", "rbi1", "rbi2", "rbi3", "rbi4", "rbi5", "rbi6", "rbi7",
};

static const char *name_rcin_bits[8] = {
	"rci0", "rci1", "rci2", "rci3", "rci4", "rci5", "rci6", "rci7",
};

static const char *name_raien_bits[8] = {
	"raien0", "raien1", "raien2", "raien3", "raien4", "raien5", "raien6", "raien7",
};

static const char *name_rbien_bits[8] = {
	"rbien0", "rbien1", "rbien2", "rbien3", "rbien4", "rbien5", "rbien6", "rbien7",
};

static const char *name_rcien_bits[8] = {
	"rcien0", "rcien1", "rcien2", "rcien3", "rcien4", "rcien5", "rcien6", "rcien7",
};

static const char *name_ioctl_bits[8] = {
	"vouten", "hdqen", "smben", "xinten", "xeven", "pwmben", "pwmaen", "32k_out",
};

static const char *name_rcpup_bits[8] = {
	"rcpup0", "rcpup1", "rcpup2", "rcpup3", "rcpup4", "rcpup5", "rcpup6", "rcpup7",
};

static const char *name_pflag_bits[8] = {
	"tmbf", "tmaf", "wkf", "timf", "ccf", "adf", "hdqf", "smbf",
};

static const char *name_pie_bits[8] = {
	"tmbie", "tmaie", "wkeve", "timie", "ccie", "adie", "hdqie", "smbie",
};

static const char *name_pctl_bits[8] = {
	"xev_edg", "xin_edg", "rsvd", "rsvd", "rsvd", "rsvd", "rsvd", "rsvd",
};

void write_io_lowlevel(u8 addr, u8 val)
{
	io_space[addr] = val;
}

u8 read_io_lowlevel(u8 addr)
{
	u8 val;
	val = io_space[addr];
	return val;
}

void write_io(u8 addr, u8 val)
{
	u8 old = io_space[addr];

	switch(addr) {
		//SMB bus region
		case 0x10:
			log_comment_add("SMBMA (Master Address)\n");
			log_comment_add("	addr=%02hhx (%s)\n",
							val, val&1?"rd":"wr");
			break;
		case 0x11:
			log_comment_add("SMBDA (Data)\n");
			log_comment_add("	data=%02hhx\n", val);
			break;
		case 0x12:
			log_comment_add("SMBACK (Ack)\n");
			log_comment_add("	%s\n",
							val&1?"acked":"weird 0");
			break;
		case 0x13:
			log_comment_add("SMBSTA (Status)\n");
			log_comment_add("	write %02hhx to readonly!\n", val);
			break;
		case 0x14:
			io_space[addr] = val;
			log_comment_reg_bits("SMBCTL (Control)", name_smbctl_bits, old, io_space[addr]);
			break;
		case 0x15:
			io_space[addr] = val & 0x7;
			log_comment_reg_bits("SMBPEC (Packet Error Check)", name_smbpec_bits, old, io_space[addr]);
			break;
		case 0x16:
			log_comment_add("SMBTAR (Target Slave Address)\n");
			log_comment_add("	addr=%02hhx (%s)\n",
							val, val&1?"rd":"wr");	//probably rd/wr
			io_space[addr] = val;
			break;
		case 0x17:
			log_comment_add("SMBBUSLO (Bus Low timeout)\n");
			if ((val & 7) == 0) {
				log_comment_add("	timeout='never'\n");
			} else {
				log_comment_add("	timeout=%d ms\n", (val & 7)*500);
			}
			io_space[addr] = val & 0x7;
			break;
		//HDQ single pin serial interface region
		case 0x20:
			log_comment_add("HDQSTA (Input and Int Control)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		case 0x21:
			io_space[addr] = val & 0x3;
			log_comment_reg_bits("HDQOUT (Output Control)", name_hdqout_bits, old, io_space[addr]);
			break;
		//ADC region
		case 0x30:
			//TODO decode chanX vector?
			io_space[addr] = val;
			log_comment_reg_bits("ADCTL0 (Control 0)", name_adctl0_bits, old, io_space[addr]);
			break;
		case 0x31:
			log_comment_add("ADCTL1 (Control 1)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		case 0x32:
			log_comment_add("ADHI (High byte)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		case 0x33:
			log_comment_add("ADMID (Middle Byte)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		case 0x34:
			log_comment_add("ADLO (Low Byte)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		//coloumb counter region
		case 0x40:
			io_space[addr] = val & 3;
			log_comment_reg_bits("CCCTL (Control)", name_ccctl_bits, old, io_space[addr]);
			break;
		case 0x41:
			log_comment_add("CCHI (High Byte)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		case 0x42:
			log_comment_add("CCLO (Low Byte)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		//PLL clock
		case 0x50:
			io_space[addr] = (old & ~3) | (val & 3);
			log_comment_reg_bits("CLK (Clock)", name_clk_bits, old & 3, io_space[addr]);
			break;
		case 0x51:
			log_comment_add("OSC_TRIM (Trim)\n");
			log_comment_add("	Trim=%d\n", val);
			io_space[addr] = val;
			break;
		case 0x52:
			log_comment_add("OSC Unknown!!\n");
			log_comment_add("	val=%02hhx\n", val);
			io_space[addr] = val;
			break;
		//timers
		case 0x60:
			if (val & 8) {
				io_space[addr] = val & 0x7;
			} else {
				io_space[addr] = (io_space[addr] & 8) | ( val & 0x7);
			}
			log_comment_reg_bits("TMRCTLW (Control)", name_tmrctlw_bits, old & 0xf, io_space[addr]);
			break;
		case 0x61:
			log_comment_add("TMRPERW (Program)\n");
			log_comment_add("	Time=%d*3.90625 ms\n", val);
			io_space[addr] = val;
			break;
		case 0x62:
			io_space[addr] = val & 0x7f;
			log_comment_reg_bits("TMRCTLA (Control A)", name_ccctl_bits, old & 0x7f, io_space[addr]);
			break;
		case 0x63:
			io_space[addr] = val;
			log_comment_reg_bits("TMRCTLB (Control B)", name_smbctl_bits, old, io_space[addr]);
			break;
		case 0x64:
			io_space[addr] = val;
			log_comment_reg_bits("TMRCTLC (Control C, Capture/Compare/PWM)", name_smbctl_bits, old, io_space[addr]);
			break;
		case 0x65:
			log_comment_add("TMRPERA (Period A)\n");
			log_comment_add("	Period=%d\n", val);
			io_space[addr] = val;
			break;
		case 0x66:
			log_comment_add("TMRPERB (Period B)\n");
			log_comment_add("	Period = %d\n", val);
			io_space[addr] = val;
			break;
		case 0x67:
			log_comment_add("TMRPWA (Pulse Width A)\n");
			log_comment_add("	Pulse Width = %d\n", val);
			io_space[addr] = val;
			break;
		case 0x68:
			log_comment_add("TMRPWB (Pulse Width B)\n");
			log_comment_add("	Pulse Width = %d\n", val);
			io_space[addr] = val;
			break;
		case 0x69:
			log_comment_add("TMRPGMA (Program A)\n");
			log_comment_add("	Init? = %d\n", val);
			io_space[addr] = val;
			break;
		case 0x6a:
			log_comment_add("TMRPGMB (Program B)\n");
			log_comment_add("	Init? = %d\n", val);
			io_space[addr] = val;
			break;
		//IO region
		case 0x70:
			io_space[addr] = val;
			log_comment_reg_bits("RA_OUT (Output A)", name_raout_bits, old, io_space[addr]);
			break;
		case 0x71:
			io_space[addr] = val;
			log_comment_reg_bits("RB_OUT (Output B)", name_rbout_bits, old, io_space[addr]);
			break;
		case 0x72:
			io_space[addr] = val;
			log_comment_reg_bits("RC_OUT (Output C)", name_rcout_bits, old, io_space[addr]);
			break;
		case 0x73:
			log_comment_add("RA_IN (Input A)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		case 0x74:
			log_comment_add("RB_IN (Input B)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		case 0x75:
			log_comment_add("RC_IN (Input C)\n");
			log_comment_add("	Write to ReadOnly reg (%02hhx)\n", val);
			break;
		case 0x76:
			io_space[addr] = val;
			log_comment_reg_bits("RA_IEN (Input Enable A)", name_raien_bits, old, io_space[addr]);
			break;
		case 0x77:
			io_space[addr] = val;
			log_comment_reg_bits("RB_IEN (Input Enable B)", name_rbien_bits, old, io_space[addr]);
			break;
		case 0x78:
			io_space[addr] = val;
			log_comment_reg_bits("RC_IEN (Input Enable C)", name_rcien_bits, old, io_space[addr]);
			break;
		case 0x79:
			io_space[addr] = val;
			log_comment_reg_bits("IOCTL (I/O Control)", name_ioctl_bits, old, io_space[addr]);
			break;
		case 0x7c:
			io_space[addr] = val;
			log_comment_reg_bits("RC_PUP (Pullup C)", name_rcpup_bits, old, io_space[addr]);
			break;
		//interrupt controller
		case 0x90:
			//TODO special masking, zero only unset bits
			io_space[addr] = old & val;
			log_comment_reg_bits("PFLAG (Periph irq flags)", name_pflag_bits, old, io_space[addr]);
			break;
		case 0x91:
			io_space[addr] = val;
			log_comment_reg_bits("PIE (Periph irq en)", name_pie_bits, old, io_space[addr]);
			sim_breakpoint_set(SIM_BREAKPOINT_DATA);
			break;
		case 0x92:
			io_space[addr] = val & 3;
			log_comment_reg_bits("PCTL (Periph Ext irq ctrl)", name_pctl_bits, old & 3, io_space[addr]);
			break;
		//voltage reference
		case 0xa0:
			log_comment_add("VTRIM (Reference voltage trim)\n");
			log_comment_add("	%d*1.4 mV (probably)\n", val & 0x3f);
			io_space[addr] = val & 0x3f;
			break;
		//TODO flash interface
		case 0x80:
			log_comment_add("Unknown flash TODO\n");
			log_comment_add("	%02hhx\n", val);
			io_space[addr] = val;
			break;
		default:
			log_comment_add("Unknown IO write TODO!!\n");
			log_comment_add("	Assuming RW register = %02hhx\n", val);
			io_space[addr] = val;
			sim_breakpoint_set(SIM_BREAKPOINT_DATA);
			break;
	}

	//TODO watch special adresses?
}

u8 read_io(u8 addr)
{
	u8 val = 0;

	switch(addr) {
		//SMB bus region
		case 0x10:
			log_comment_add("SMBMA (Master Address)\n");
			log_comment_add("	Read of WriteOnly reg\n");
			val = 0;
			break;
		case 0x11:
			log_comment_add("SMBDA (Data)\n");
			val = io_space[addr];
			log_comment_add("	data=%02hhx\n", val);
			break;
		case 0x12:
			log_comment_add("SMBACK (Ack)\n");
			log_comment_add("	Read of WriteOnly reg\n");
			val = 0;
			break;
		case 0x13:
			val = io_space[addr];
			log_comment_reg_bits("SMBSTA (Stat)", name_smbsta_bits, val, val);
			break;
		case 0x14:
			val = io_space[addr];
			log_comment_reg_bits("SMBCTL (Ctrl)", name_smbctl_bits, val, val);
			break;
		case 0x15:
			val = io_space[addr] & 7;
			log_comment_reg_bits("SMBPEC (Packet Error Check)", name_smbpec_bits, val, val);
			break;
		case 0x16:
			val = io_space[addr];
			log_comment_add("SMBTAR (Target Slave Addr)\n");
			log_comment_add("	addr=%02hhx (%s)\n",
							val, val&1?"rd":"wr");	//probably rd/wr
			break;
		case 0x17:
			val = io_space[addr];
			log_comment_add("SMBBUSLO (Bus Low timeout)\n");
			if ((val & 7) == 0) {
				log_comment_add("	timeout='never'\n");
			} else {
				log_comment_add("	timeout=%d ms\n", (val & 7)*500);
			}
			break;
		//HDQ single pin serial interface region
		case 0x20:
			val = io_space[addr] & 0xf;
			log_comment_reg_bits("HDQSTA (Input and irq ctrl)", name_hdqsta_bits, val, val);
			break;
		case 0x21:
			val = io_space[addr] & 0x3;
			log_comment_reg_bits("HDQOUT (Output ctrl)", name_hdqout_bits, val, val);
			break;
		//ADC region
		case 0x30:
			//TODO decode chanX vector?
			val = io_space[addr];
			log_comment_reg_bits("ADCTL0 (ctrl 0)", name_adctl0_bits, val, val);
			break;
		case 0x31:
			val = io_space[addr] & 1;
			log_comment_add("ADCTL1 (Control 1)\n");
			log_comment_add("	ready = %02hhx\n", val);
			break;
		case 0x32:
			val = io_space[addr] | 0x80;
			log_comment_add("ADHI (High byte)\n");
			log_comment_add("	ADC measurement = %d\n",
							(io_space[0x32] << 16) |
							(io_space[0x33] << 8) |
							io_space[0x34]);
			break;
		case 0x33:
			val = io_space[addr];
			log_comment_add("ADMID (Middle Byte)\n");
			log_comment_add("	ADC measurement = %d\n",
							(io_space[0x32] << 16) |
							(io_space[0x33] << 8) |
							io_space[0x34]);
			break;
		case 0x34:
			val = io_space[addr];
			log_comment_add("ADLO (Low Byte)\n");
			log_comment_add("	ADC measurement = %d\n",
							(io_space[0x32] << 16) |
							(io_space[0x33] << 8) |
							io_space[0x34]);
			break;
		//coloumb counter region
		case 0x40:
			val = io_space[addr] & 3;
			log_comment_reg_bits("CCCTL (Coulomb Ctrl)", name_ccctl_bits, val, val);
			break;
		case 0x41:
			val = io_space[addr];
			log_comment_add("CCHI (High Byte)\n");
			log_comment_add("	Coulomb measurement = %hd\n",
							(io_space[0x41] << 8) |
							io_space[0x42]);
			break;
		case 0x42:
			val = io_space[addr];
			log_comment_add("CCLO (Low Byte)\n");
			log_comment_add("	Coulomb measurement = %hd\n",
							(io_space[0x41] << 8) |
							io_space[0x42]);
			break;
		//PLL clock
		case 0x50:
			val = io_space[addr];
			log_comment_reg_bits("CLK (Clock)", name_clk_bits, val, val);
			break;
		case 0x51:
			val = io_space[addr];
			log_comment_add("OSC_TRIM (Trim)\n");
			log_comment_add("	Trim=%d\n", val);
			break;
		case 0x52:
			//NOTICE sim writes 0x6b, IRL read out 0x0b
			val = io_space[addr] & 0x0f;
			log_comment_add("OSC Unknown!!\n");
			log_comment_add("	val=%02hhx\n", val);
			break;
		//timers
		case 0x60:
			val = io_space[addr] & 0xf;
			log_comment_reg_bits("TMRCTLW (Ctrl)", name_tmrctlw_bits, val, val);
			break;
		case 0x61:
			val = io_space[addr];
			log_comment_add("TMRPERW (Program)\n");
			log_comment_add("	Time=%d*3.90625 ms\n", val);
			break;
		case 0x62:
			val = io_space[addr] & 0x7f;
			log_comment_reg_bits("TMRCTLA (Ctrl A)", name_tmrctla_bits, val, val);
			break;
		case 0x63:
			val = io_space[addr];
			log_comment_reg_bits("TMRCTLB (Ctrl B)", name_tmrctlb_bits, val, val);
			break;
		case 0x64:
			val = io_space[addr];
			log_comment_reg_bits("TMRCTLC (Ctrl C, Capture/Compare/PWM)", name_tmrctlc_bits, val, val);
			break;
		case 0x65:
			val = io_space[addr];
			log_comment_add("TMRPERA (Period A)\n");
			log_comment_add("	Period=%d\n", val);
			break;
		case 0x66:
			val = io_space[addr];
			log_comment_add("TMRPERB (Period B)\n");
			log_comment_add("	Period = %d\n", val);
			break;
		case 0x67:
			val = io_space[addr];
			log_comment_add("TMRPWA (Pulse Width A)\n");
			log_comment_add("	Pulse Width = %d\n", val);
			break;
		case 0x68:
			val = io_space[addr];
			log_comment_add("TMRPWB (Pulse Width B)\n");
			log_comment_add("	Pulse Width = %d\n", val);
			break;
		case 0x69:
			val = io_space[addr];
			log_comment_add("TMRPGMA (Program A)\n");
			log_comment_add("	Init? = %d\n", val);
			break;
		case 0x6a:
			val = io_space[addr];
			log_comment_add("TMRPGMB (Program B)\n");
			log_comment_add("	Init? = %d\n", val);
			break;
		//IO region
		case 0x70:
			val = io_space[addr];
			log_comment_reg_bits("RA_OUT (Output A)", name_raout_bits, val, val);
			break;
		case 0x71:
			val = io_space[addr];
			log_comment_reg_bits("RB_OUT (Output B)", name_rbout_bits, val, val);
			break;
		case 0x72:
			val = io_space[addr];
			log_comment_reg_bits("RC_OUT (Output C)", name_rcout_bits, val, val);
			break;
		case 0x73:
			val = io_space[addr];
			log_comment_reg_bits("RA_IN (Input A)", name_rain_bits, val, val);
// 			sim_breakpoint_set(SIM_BREAKPOINT_DATA);
			break;
		case 0x74:
			val = io_space[addr];
			log_comment_reg_bits("RB_IN (Input B)", name_rbin_bits, val, val);
// 			sim_breakpoint_set(SIM_BREAKPOINT_DATA);
			break;
		case 0x75:
			val = io_space[addr];
			log_comment_reg_bits("RC_IN (Input C)", name_rcin_bits, val, val);
// 			sim_breakpoint_set(SIM_BREAKPOINT_DATA);
			break;
		case 0x76:
			val = io_space[addr];
			log_comment_reg_bits("RA_IEN (Input Enable A)", name_raien_bits, val, val);
			break;
		case 0x77:
			val = io_space[addr];
			log_comment_reg_bits("RB_IEN (Input Enable B)", name_rbien_bits, val, val);
			break;
		case 0x78:
			val = io_space[addr];
			log_comment_reg_bits("RC_IEN (Input Enable C)", name_rcien_bits, val, val);
			break;
		case 0x79:
			val = io_space[addr];
			log_comment_reg_bits("IOCTL (I/O Ctrl)", name_ioctl_bits, val, val);
			break;
		case 0x7c:
			val = io_space[addr];
			log_comment_reg_bits("RC_PUP (Pullup C)", name_rcpup_bits, val, val);
			break;
		//interrupt controller
		case 0x90:
			val = io_space[addr];
			log_comment_reg_bits("PFLAG (Periph irq flags)", name_pflag_bits, val, val);
			break;
		case 0x91:
			val = io_space[addr];
			log_comment_reg_bits("PIE (Periph irq enable)", name_pie_bits, val, val);
			break;
		case 0x92:
			val = io_space[addr] & 3;
			log_comment_reg_bits("PCTL (Periph Ext irq ctrl)", name_pctl_bits, val, val);
			break;
		//voltage reference
		case 0xa0:
			val = io_space[addr] & 0x3f;
			log_comment_add("VTRIM (Reference voltage trim)\n");
			log_comment_add("	%d*1.4 mV (probably)\n", val);
			break;
		//TODO flash interface
		case 0x80:
			val = io_space[addr];
			log_comment_add("Unknown flash TODO\n");
			log_comment_add("	%02hhx\n", val);
			break;
		default:
			val = io_space[addr];
			log_comment_add("!!Unknown IO write TODO!!\n");
			log_comment_add("	Assuming RW register = %02hhx\n", val);
			break;
	}

	//TODO watch special adresses?
	return val;
}

void init_ioregs(char * filename)
{
	memset(io_space, 0, sizeof(io_space));

	if (filename) {
		FILE * fp;
		fp = fopen(filename, "r");
		if (fp == NULL) {
			perror("IO dump opening");
			exit(1);
		}
		fread(io_space, 0x100, 1, fp);
		fclose(fp);
	}

	//SMBSTA, BUS_FREE
	io_space[0x13] = 0x4;
	//SMBBUSLO, BUSLO2=1 during reset
	io_space[0x17] = 0x4;
	//ADCTL1 bit7 =1
	io_space[0x31] = 0x80;
	//ADHI bit7 =1
	io_space[0x32] = 0x80;
	//CLK OSC_EN=1, PLL_EN=1
	io_space[0x50] = 0x3;
	//TMRCTLW WDCRST=1
	io_space[0x60] = 0x1;
	//TMRPERW
	io_space[0x61] = 0xff;
	//TMRPERA
	io_space[0x65] = 0xff;
	//TMRPERB
	io_space[0x66] = 0xff;
	//TMRPWA
	io_space[0x67] = 0xff;
	//TMRPWB
	io_space[0x68] = 0xff;
	//RA_OUT
	io_space[0x70] = 0xff;
	//RB_OUT
	io_space[0x71] = 0xff;
	//RC_OUT
	io_space[0x72] = 0xff;

	//PIE
// 	io_space[0x90] |= 0x08;

}
