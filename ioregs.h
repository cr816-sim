#ifndef IOREGS_H
#define IOREGS_H

#include "disasm.h"

void write_io(u8 addr, u8 val);
u8 read_io(u8 addr);

void write_io_lowlevel(u8 addr, u8 val);
u8 read_io_lowlevel(u8 addr);

void init_ioregs(char * filename);

#endif
