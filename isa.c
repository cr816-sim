#include <string.h>
#include <stdlib.h>
#include "disasm.h"
#include "log.h"
#include "access.h"

static u16 hw_stack[STACK_LEN];
static struct opcode_word mem_prog[MEM_PROG_LEN];


#define IXS_NAME_MAX 4
static const char ixs_to_name[4][IXS_NAME_MAX] = {
	"i0", "i1", "i2", "i3",
};

#define REG_NAME_MAX 6
static const char regs_to_name[16][REG_NAME_MAX] = {
	"i0l", "i0h", "i1l", "i1h",
	"i2l", "i2h", "i3l", "i3h",
	"ipl", "iph", "stat", "r3",
	"r2", "r1", "r0", "a",
};

const char * get_ixs_name(enum alu_ixs ixs)
{
	return ixs_to_name[ixs];
}

const char * get_regs_name(enum alu_regs regs)
{
	return regs_to_name[regs];
}

void do_push(u16 val)
{
	for (unsigned idx=STACK_LEN-1;idx>0;idx--) {
		hw_stack[idx] = hw_stack[idx-1];
	}
	hw_stack[0] = val;
}

u16 do_pop(void)
{
	u16 ret;

	ret = hw_stack[0];
	for (unsigned idx=0;idx<STACK_LEN-1;idx++) {
		hw_stack[idx] = hw_stack[idx+1];
	}

	return ret;
}


static u16 do_alu_testb(u8 op, u8 val)
{
	u8 acc;
	struct cpuflags flags = read_flags(0);

#ifdef QUIRK_TESTB_IS_AND
	//reality
	acc = op & val;
#else
	//specification
	acc = op & (1 << (val & 7));
#endif

	flags.z = (acc == 0)?1:0;

	write_flags(flags, 1);
	write_reg8(MAIN_REG_A, acc, 1);

	return read_pc()+1;
}

//move to memory
static u16 do_move_mem(u16 dest, u8 src)
{
	log_instr_name("move");

	write_mem(dest, src, 1);

	return read_pc()+1;
}

#define OP5_NAME_MAX 12
static const char op5_to_name[32][OP5_NAME_MAX] = {
	//0 - 7
	"cmpa", "cmp", "and", "subs", "subd", "subdc", "mula", "subsc",
	//8 - 0xf
	"xor", "unknown_09", "move", "or", "add", "addc", "mul", "unknown_0f",
	//0x10 - 0x17
	"shra", "inc", "cmvd", "cmvs", "shrc", "incc", "shr", "unknown_17",
	//0x18 - 0x1f
	"cpl1", "cpl2", "shl", "dec", "cpl2c", "unknown_1d","shlc", "decc"
};


//indexed alu with immediate
//ex: op1 - op2
static u16 do_alu_op5(
	enum alu_op5 alu, enum alu_regs dest, u8 op1, enum alu_regs op2_enum)
{
	s16 s16temp;
	u16 u16temp;
	u8 acc;
	u8 op2;
	struct cpuflags flags = read_flags(0);
	//TODO log only where required (move into cases)

	log_instr_name("%s", op5_to_name[alu]);

	switch(alu) {
		case OP5_CMPA:
			//ex op1 = #0x62
			op2 = read_reg8(op2_enum, 1);
			acc = (u8)((s8)(op1) - (s8)(op2));

 			flags.c = (((s8)(op2)) > ((s8)(op1)))?0:1;
			flags.z = (acc == 0)?1:0;
			flags.v = flags.c && (!flags.z);

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			break;
		case OP5_CMP:
			op2 = read_reg8(op2_enum, 1);

			acc = op1 - op2;

			flags.c = (op2 > op1)?0:1;
			flags.z = (acc == 0)?1:0;
			flags.v = flags.c && (!flags.z);

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			break;
		case OP5_AND:
			op2 = read_reg8(op2_enum, 1);

			acc = op1 & op2;

			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			//TODO global function?
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_SUBS:
			op2 = read_reg8(op2_enum, 1);

			acc = (u8)((s16)op2 - (s16)op1);

			//TODO maybe better eval?
			if ((((s16)((s8)(op2)) - (s16)((s8)(op1))) < -0x80) ||
				(((s16)((s8)(op2)) - (s16)((s8)(op1))) > 0x7f)) {
				flags.v = 1;
			} else {
				flags.v = 0;
			}
			flags.c = (op1 > op2)?0:1;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_SUBD:
			op2 = read_reg8(op2_enum, 1);

			acc = (u8)((s16)op1 - (s16)op2);

			//TODO maybe better eval?
			if ((((s16)((s8)(op1)) - (s16)((s8)(op2))) < -0x80) ||
				(((s16)((s8)(op1)) - (s16)((s8)(op2))) > 0x7f)) {
				flags.v = 1;
			} else {
				flags.v = 0;
			}
			flags.c = (op2 > op1)?0:1;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_SUBDC:
			op2 = read_reg8(op2_enum, 1);

			//TODO maybe better flag evaluation?
			acc = (s16)op1 - (s16)op2 - (flags.c?0:1);

			if ((((s16)((s8)(op1)) - (s16)((s8)(op2))) < -0x80) ||
				(((s16)((s8)(op1)) - (s16)((s8)(op2))) > 0x7f)) {
				flags.v = 1;
			} else {
				flags.v = 0;
			}

			flags.c = (op2 > op1)?0:1;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_MULA:
			op2 = read_reg8(op2_enum, 1);

			s16temp = ((s16)op1) * ((s16)op2);
			acc = s16temp & 0xff;

			write_reg8(MAIN_REG_A, acc, 1);
			write_reg8(dest, s16temp >> 8, 1);
			break;
		case OP5_SUBSC:
			op2 = read_reg8(op2_enum, 1);

			acc = (s16)op2 - (s16)op1 - (flags.c?0:1);

			//TODO maybe better eval?
			if ((((s16)((s8)(op2)) - (s16)((s8)(op1))) < -0x80) ||
				(((s16)((s8)(op2)) - (s16)((s8)(op1))) > 0x7f)) {
				flags.v = 1;
			} else {
				flags.v = 0;
			}
			flags.c = (op1 > op2)?0:1;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_XOR:
			op2 = read_reg8(op2_enum, 1);

			acc = op1 ^ op2;

			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_MOVE:
			acc = op1;

			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_OR:
			op2 = read_reg8(op2_enum, 1);

			acc = op1 | op2;

			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_ADD:
			op2 = read_reg8(op2_enum, 1);

			//TODO double reg read, read only once!!
			u16temp = op1 + op2;
			acc = u16temp & 0xff;

// fprintf(stderr, " \n||%02x %02x|| ", op1, op2);

			if (((s8)op1 > 0) && ((s8)op2 > 0)) {
flags.v = ((s8)u16temp < 0)?1:0;
// 			fprintf(stderr, " >>>A %02x %02x<<<\n", u16temp, s16temp);
			} else if (((s8)op1 < 0) && ((s8)op2 < 0)) {
flags.v = ((s8)u16temp > 0)?1:0;
// 			fprintf(stderr, " >>>B %02x %02x<<<\n", u16temp, s16temp);
			} else {
flags.v = 0;
// 			fprintf(stderr, " >>>C %02x %02x<<<\n", u16temp, s16temp);
			}
			flags.c = (u16temp > 0xff)?1:0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_ADDC:
			op2 = read_reg8(op2_enum, 1);

			//TODO testing carry
			u16temp = op1 + op2 + (flags.c?1:0);
			acc = u16temp & 0xff;

			//TODO carry?
			if (((s8)op1 > 0) && ((s8)op2 > 0)) {
flags.v = ((s8)u16temp < 0)?1:0;
// 			fprintf(stderr, " >>>A %02x %02x<<<\n", u16temp, s16temp);
			} else if (((s8)op1 < 0) && ((s8)op2 < 0)) {
flags.v = ((s8)u16temp > 0)?1:0;
// 			fprintf(stderr, " >>>B %02x %02x<<<\n", u16temp, s16temp);
			} else {
flags.v = 0;
// 			fprintf(stderr, " >>>C %02x %02x<<<\n", u16temp, s16temp);
			}
// 			flags.v = (u16temp > 0x7f)?1:0;
			flags.c = (u16temp > 0xff)?1:0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_MUL:
			op2 = read_reg8(op2_enum, 1);

			u16temp = ((s16)op1) * ((s16)op2);
			acc = u16temp & 0xff;

			write_reg8(MAIN_REG_A, acc, 1);
			write_reg8(dest, u16temp >> 8, 1);
			break;
		case OP5_SHRA:
			acc = ((op1 >> 1) & 0x7f) | (op1 & 0x80);

			flags.c = (op1 & 1)?1:0;
			flags.v = 0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_INC:
			acc = op1 + 1;

			flags.v = (op1 == 0x7f)?1:0;
			flags.c = (acc == 0)?1:0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_CMVD:
			//TODO log if depends on flag state
			flags.z = (op1 == 0)?1:0;	//always modified

			if (!flags.c) {
				acc = op1;

				write_reg8(MAIN_REG_A, acc, 1);
				if (dest != MAIN_REG_A)
					write_reg8(dest, acc, 1);
			}

			write_flags(flags, 1);
			break;
		case OP5_CMVS:
			flags.z = (op1 == 0)?1:0;	//always modified

			if (flags.c) {
				acc = op1;

				write_reg8(MAIN_REG_A, acc, 1);
				if (dest != MAIN_REG_A)
					write_reg8(dest, acc, 1);
			}

			write_flags(flags, 1);
			break;
		case OP5_SHRC:
			acc = ((op1 >> 1) & 0x7f) | (flags.c?0x80:0);

			flags.c = (op1 & 1)?1:0;
			flags.v = 0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_INCC:
			//TODO testing carry
			acc = (op1) + (flags.c?1:0);

			flags.v = ((op1 == 0x7f)&&(flags.c))?1:0;
			flags.c = (acc == 0)?1:0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_SHR:
			acc = ((op1 >> 1) & 0x7f);

			flags.v = 0;
			flags.c = (op1 & 1)?1:0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_CPL1:
			acc = (~ op1);

			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_CPL2:
			acc = (~ op1) + 1;

			flags.v = (op1 == 0x80)?1:0;
			flags.c = (op1 == 0)?1:0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_SHL:
			acc = ((op1 << 1) & 0xfe);

			flags.v = (((op1 >> 7)&1)!=((op1 >> 6)&1))?1:0;
			flags.c = (op1 & 0x80)?1:0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_DEC:
			acc = op1 - 1;

			flags.v = (op1 == 0x80)?1:0;
			flags.c = (acc == 0xff)?0:1;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_CPL2C:
			acc = (~ op1) + (flags.c?1:0);

			flags.v = ((op1 == 0x80)&&(flags.c))?1:0;
			flags.c = ((op1 == 0)&&(flags.c))?1:0;	//NOTICE causal
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_SHLC:
			acc = ((op1 << 1) & 0xfe) | (flags.c?1:0);

			flags.v = (((op1 >> 7)&1)!=((op1 >> 6)&1))?1:0;
			flags.c = (op1 & 0x80)?1:0;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		case OP5_DECC:
			//TODO testing carry
			acc = op1 + (flags.c?1:0) - 1;

			flags.v = (acc == 0x7f)?1:0;
			flags.c = (acc == 0xff)?0:1;
			flags.z = (acc == 0)?1:0;

			write_flags(flags, 1);
			write_reg8(MAIN_REG_A, acc, 1);
			if (dest != MAIN_REG_A)
				write_reg8(dest, acc, 1);
			break;
		default:
			sim_breakpoint_set(SIM_BREAKPOINT_CODE);
			break;
	}

	return read_pc()+1;
}


#define JMP_NAME_MAX 6
static const char jmp_type_name[8][JMP_NAME_MAX] = {
	"jcc", "jvc", "jzc", "jmp",
	"jcs", "jvs", "jzs", "jevt",
};


static unsigned do_jump(enum jmp_type cc) {
	struct cpuflags f;
	struct reg_stat stat;

	switch(cc) {
	case JMP_CC:
		f = read_flags(1);
		log_access_read_add("(> !c) ");
		return (!f.c)?1:0;
	case JMP_VC:
		f = read_flags(1);
		log_access_read_add("(>= !o) ");
		return (!f.v)?1:0;
	case JMP_ZC:
		f = read_flags(1);
		log_access_read_add("(!= !z) ");
		return (!f.z)?1:0;
	case JMP_UNCOND:
		return 1;
	case JMP_CS:
		f = read_flags(1);
		log_access_read_add("(<= C) ");
		return (f.c)?1:0;
	case JMP_VS:
		f = read_flags(1);
		log_access_read_add("(< O) ");
		return (f.v)?1:0;
	case JMP_ZS:
		f = read_flags(1);
		log_access_read_add("(== Z) ");
		return (f.z)?1:0;
	case JMP_EVT:
		stat.raw = read_reg8(MAIN_REG_STAT, 1);
		log_access_read_add("(event) ");
		return (stat.ev0 || stat.ev1)?1:0;
	}
	return 0;	//default error?
}

/** ******** opcodes *******/

static u16 opcode_jump_imm(struct opcode_word opcode)
{
	u16 addr = (~opcode.jmp_imm.n_addr) & 0xffff;
	enum jmp_type cc = opcode.jmp_imm.cc;

	log_instr_name("%s", jmp_type_name[cc]);
	log_instr_args("0x%04hx", addr);

	if (do_jump(cc)) {
		//TODO append?
// 		fprintf(stderr, "GO");
		return addr;
	} else {
// 		fprintf(stderr, "SKIP");
		return read_pc()+1;
	}
}

static u16 opcode_jump_ip(struct opcode_word opcode)
{
	u16 addr = read_ip(1);
	enum jmp_type cc = opcode.jmp_imm.cc;	//borrowed?

	log_instr_name("%s", jmp_type_name[cc]);
	log_instr_args("ip");

	if (do_jump(cc)) {
		//TODO use vvv?
// 		log_comment_add("GO");
		return addr;
	} else {
// 		log_comment_add("SKIP");
		return read_pc()+1;
	}
}

static u16 opcode_nop(struct opcode_word opcode)
{
	log_instr_name("nop");
	return read_pc()+1;
}

static u16 opcode_ret(struct opcode_word opcode)
{
	log_instr_name("ret");
	return do_pop();
}

static u16 opcode_reti(struct opcode_word opcode)
{
	log_instr_name("reti");

	struct reg_stat stat;
	stat.raw = read_reg8(MAIN_REG_STAT, 1);
	stat.gie = 1;
	write_reg8(MAIN_REG_STAT, stat.raw, 1);

	return do_pop();
}

static u16 opcode_pop(struct opcode_word opcode)
{
	log_instr_name("pop");

	write_ip(do_pop(), 1);

	return read_pc()+1;
}

static u16 opcode_push(struct opcode_word opcode)
{
	log_instr_name("push");

	do_push(read_ip(1));

	return read_pc()+1;
}

static u16 opcode_call_imm(struct opcode_word opcode)
{
	u16 addr = ~(opcode.call_imm.n_addr & 0xffff);

	log_instr_name("call");
	log_instr_args("0x%04hx", addr);

	do_push(read_pc() + 1);

	return addr;
}

static u16 opcode_call_ip(struct opcode_word opcode)
{
	u16 addr = read_ip(1);

	log_instr_name("call");
	log_instr_args("ip");

	do_push(read_pc() + 1);

	return addr;
}

static u16 opcode_calls_imm(struct opcode_word opcode)
{
	u16 addr = ~(opcode.calls_imm.n_addr & 0xffff);

	log_instr_name("calls");
	log_instr_args("0x%04hx", addr);

	write_ip(read_pc() + 1, 1);
	return addr;
}

static u16 opcode_calls_ip(struct opcode_word opcode)
{
	u16 addr = read_ip(1);

	log_instr_name("calls");
	log_instr_args("ip");

	write_ip(read_pc() + 1, 1);
	return addr;
}

static u16 opcode_pmd(struct opcode_word opcode)
{
	//TODO
	log_instr_name("pmd");

	if (opcode.pmd.on) {
		log_instr_args("on");
	} else {
		log_instr_args("off");
	}

	return read_pc()+1;
}

static u16 opcode_halt(struct opcode_word opcode)
{
	struct reg_stat stat;
	stat.raw = read_reg8(MAIN_REG_STAT, 1);

	log_instr_name("halt");

	sim_breakpoint_set(SIM_BREAKPOINT_HALT);

	if (stat.ev0 || stat.ev1) {

//experiment
// 		stat.ev0 = 0;
// 		stat.ev1 = 0;
// 	write_reg8(MAIN_REG_STAT, stat.raw, 1);


		return read_pc()+1;
	}


	return read_pc();
}

static u16 opcode_freq(struct opcode_word opcode)
{
	log_instr_name("freq");

	switch(opcode.freq.div) {
		case 0x0:
			log_instr_args("clk");
			break;
		case 0x8:
			log_instr_args("clk/2");
			break;
		case 0xc:
			log_instr_args("clk/4");
			break;
		case 0xe:
			log_instr_args("clk/8");
			break;
		case 0xf:
			log_instr_args("clk/16");
			break;
		default:
			log_instr_args("invalid div table = %01hhx",
				opcode.freq.div);
			break;
	}

	return read_pc()+1;
}

static u16 opcode_sflag(struct opcode_word opcode)
{
	struct cpuflags f = read_flags(1);

	log_instr_name("sflag");

	u8 acc;

	acc =
		((f.c&1) << 7) |
		(((f.c ^ f.v)&1) << 6) |
		((f.sf&1) << 5) |
		((f.se&1) << 4);
	//bit 3 = was halt (only 816l?)

	if (acc == 0) {
		//816l no Z modify
		//rest bit, undefined -> can be "1" -> never
		f.z = 1;
	}

	write_reg8(MAIN_REG_A, acc, 1);

	return read_pc()+1;
}


//alu1 rx, (ix, 0x12)
static u16 opcode_alu1(struct opcode_word opcode)
{
	enum alu_op5 alu = opcode.alu1.alu_op;
	enum alu_ixs ixs = opcode.alu1.ix;
	enum alu_regs dst = opcode.alu1.dst;
	u8 offset = opcode.alu1.offset;

	log_instr_args("%s, (%s, 0x%02hhx)",
				   get_regs_name(dst),
				   get_ixs_name(ixs),
				   offset
	);

	u16 addr = read_ix(ixs, 1) + offset;

	return do_alu_op5(
		alu,
		dst,	//destination
		read_mem(addr, 1),
 		dst
	);
}

//alu2 rx, -(ix, 0x12)+
static u16 opcode_alu2(struct opcode_word opcode)
{
	enum alu_op5 alu = opcode.alu2.alu_op;
	enum alu_ixs ixs = opcode.alu2.ix;
	enum alu_regs dst = opcode.alu2.dst;
	unsigned sign = opcode.alu2.sign;
	u8 cpl2_offset = opcode.alu2.cpl2_offset;

	u16 addr, newix;
	u16 ret;

	if (sign) {
		//pre, signed
		log_instr_args("%s, -(%s, 0x%02hhx)",
			get_regs_name(dst), get_ixs_name(ixs), cpl2_offset);

 		addr = read_ix(ixs, 1) + ((s16)(cpl2_offset) | 0xff80);
		newix = addr;
	} else {
		//post, unsigned
		log_instr_args("%s, (%s, 0x%02hhx)+",
			   get_regs_name(dst), get_ixs_name(ixs), cpl2_offset);

		addr = read_ix(ixs, 1);
		newix = addr + cpl2_offset;
	}

	ret = do_alu_op5(
		alu,
		dst,
		read_mem(addr, 1),
		dst
	);

	//NOTICE problem with modification causality (opcode i0l, -(i0, 42)) ... everything is: stores new index value after the memory access is done
	write_ix(ixs, newix, 1);

	return ret;
}

#define OP4_NAME_MAX 12
static const char op4_to_name[16][OP4_NAME_MAX] = {
	//0 - 7
	"cmpa", "cmp", "and", "subs", "subd", "subdc", "mula", "subsc",
	//8 - 0xf
	"xor", "unknown_09", "move", "or", "add", "addc", "mul", "tstb",
};

//alu3 rx, #const
static u16 opcode_alu3(struct opcode_word opcode)
{
	enum alu_op4 alu = opcode.alu3.alu_op;
	enum alu_regs dst = opcode.alu3.dst;
	u8 val = (~opcode.alu3.n_data) & 0xff;

	log_instr_name("%s", op4_to_name[alu]);

	log_instr_args("%s, #0x%02hhx", get_regs_name(dst), val);

	if (alu == OP4_TESTB) {
		return do_alu_testb(read_reg8(dst, 1), val);
	} else {
		return do_alu_op5(
			(enum alu_op5)alu,
			dst,
			val,
			dst
		);
	}
}

//only register operands
static u16 opcode_alu4(struct opcode_word opcode)
{
	enum alu_op5 alu = opcode.alu4.alu_op;
	enum alu_regs reg_op2_k = opcode.alu4.op2;
	enum alu_regs reg_op1_j = opcode.alu4.op1;
	enum alu_regs reg_res_i = opcode.alu4.dst;

	switch(alu) {
		case OP5_CMPA:
		case OP5_CMP:
			//alu4 op1 vs op2
			log_instr_args("%s, %s",
					get_regs_name(reg_op1_j),
					get_regs_name(reg_op2_k));

			if (reg_res_i != 0xf) {
				log_comment_add("bad reg field I/res %s",
						get_regs_name(reg_res_i));
			}
			break;
		case OP5_AND:
		case OP5_SUBS:
		case OP5_SUBD:
		case OP5_SUBDC:
		case OP5_MULA:
		case OP5_SUBSC:
		case OP5_OR:
		case OP5_ADD:
		case OP5_ADDC:
		case OP5_MUL:
			//alu4 regi, regj, regk
			log_instr_args("%s, %s, %s",
				   get_regs_name(reg_res_i),
				   get_regs_name(reg_op1_j),
				   get_regs_name(reg_op2_k));
			break;
		case OP5_MOVE:	//probably
		case OP5_SHRA:
		case OP5_INC:
		case OP5_CMVD:
		case OP5_CMVS:
		case OP5_SHRC:
		case OP5_INCC:
		case OP5_SHR:
		case OP5_CPL1:
		case OP5_CPL2:
		case OP5_SHL:
		case OP5_DEC:
		case OP5_CPL2C:
		case OP5_SHLC:
		case OP5_DECC:
			//alu4 regi=f(regj)
			log_instr_args("%s, %s",
					get_regs_name(reg_res_i),
					get_regs_name(reg_op1_j));

			if (reg_op2_k != 0xf) {
				log_comment_add("bad regk array %s",
					   get_regs_name(reg_op2_k));
			}
			break;
		default:
			//alu4 regi, regj, regk
			log_instr_args("%s, %s, %s",
				   get_regs_name(reg_res_i),
				   get_regs_name(reg_op1_j),
				   get_regs_name(reg_op2_k));
			log_comment_add("unknown reg use");
			break;
	}

	return do_alu_op5(
		alu,
		reg_res_i,
		read_reg8(reg_op1_j, 1),
		reg_op2_k
	);
}

//alu5 regi, (ix, r3)
static u16 opcode_alu5(struct opcode_word opcode)
{
	enum alu_op5 alu = opcode.alu5.alu_op;
	enum alu_ixs ixs = opcode.alu5.ix;
	enum alu_regs dst = opcode.alu5.dst;
	u16 addr = read_ix(ixs, 1) + read_reg8(MAIN_REG_R3, 1);

	log_instr_args("%s, (%s, r3)",
		get_regs_name(dst), get_ixs_name(ixs));

	return do_alu_op5(
		alu,
		dst,
		read_mem(addr, 1),
		dst
	);
}

//alu6 regi, direct addr
static u16 opcode_alu6(struct opcode_word opcode)
{
	enum alu_op5 alu = opcode.alu6.alu_op;
	enum alu_regs dst = opcode.alu6.dst;
	u16 addr = (~opcode.alu6.n_addr) & 0xff;

	log_instr_args("%s, 0x%02hhx",
		get_regs_name(dst), addr);

	return do_alu_op5(
		alu,
		dst,
		read_mem(addr, 1),
		dst
	);
}

//move7 (ix, r3), reg
static u16 opcode_move7(struct opcode_word opcode)
{
	enum alu_ixs ixs = opcode.move7.ix;
	enum alu_regs src = opcode.move7.src;
	u16 addr = read_ix(ixs, 1) + read_reg8(MAIN_REG_R3, 1);

	log_instr_args("(%s, r3), %s",
		get_ixs_name(ixs), get_regs_name(src));

	return do_move_mem(
		addr,
		read_reg8(src, 1)
	);
}

static u16 opcode_move8(struct opcode_word opcode)
{
	enum alu_ixs ixs = opcode.move8.ix;
	enum alu_regs src = opcode.move8.src;
	u8 cpl2_offset = opcode.move8.cpl2_offset;
	u16 addr, newix;
	u16 ret;

	if (opcode.move8.sign) {
		//pre/signed: move8 -(ix, 0x12), reg
		log_instr_args("-(%s, 0x%02hhx), %s",
			get_ixs_name(ixs), cpl2_offset, get_regs_name(src));

 		addr = read_ix(ixs, 1) + ((s16)(cpl2_offset) | 0xff80);

		newix = addr;
	} else {
		//post/unsigned: move8 (ix, 0x12)+, reg
		log_instr_args("(%s, 0x%02hhx)+, %s",
			get_ixs_name(ixs), cpl2_offset, get_regs_name(src));

		addr = read_ix(ixs, 1);

		newix = addr + cpl2_offset;
	}

	ret = do_move_mem(
		addr,
		read_reg8(src, 1)
	);

	//NOTICE problem with modification causality (opcode i0l, -(i0, 42)) ... everything is: stores new index value after the memory access is done
	write_ix(ixs, newix, 1);

	return ret;
}

//move9 (ix, 0xff), reg
static u16 opcode_move9(struct opcode_word opcode)
{
	enum alu_ixs ixs = opcode.move9.ix;
	enum alu_regs src = opcode.move9.src;
	u16 addr = read_ix(ixs, 1) + opcode.move9.offset;

	log_instr_args("(%s, 0x%02hhx), %s",
		   get_ixs_name(ixs), opcode.move9.offset, get_regs_name(src));

	return do_move_mem(
		addr,
		read_reg8(src, 1)
	);
}

//move10 0xff, reg (direct address)
static u16 opcode_move10(struct opcode_word opcode)
{
	enum alu_regs src = opcode.move10.src;
	u8 addr = (~opcode.move10.n_addr) & 0xff;

	log_instr_args("0x%02hhx, %s",
		   addr, get_regs_name(src));

	return do_move_mem(
		addr,
		read_reg8(src, 1)
	);
}

//move11 0xff, #0x42
static u16 opcode_move11(struct opcode_word opcode)
{
	u8 addr = (~opcode.move11.n_addr) & 0xff;
	u8 val = (~opcode.move11.n_data) & 0xff;

	log_instr_args("0x%02hhx, #0x%02hhx",
		   addr, val);

	return do_move_mem(
		addr,
		val
	);
}

//invalid opcode
static u16 opcode_inval(struct opcode_word opcode)
{
	log_instr_name("!inval %06x", opcode.raw);
	return read_pc()+1;	//??
}


//83p coolrisc trm
#define DECODER_ALL_BITS 0x3fffff
static const struct opcode_decoder cr816[] = {
	{DECODER_ALL_BITS, DECODER_ALL_BITS, opcode_nop},
	{DECODER_ALL_BITS, 0x3f3fff, opcode_ret},
	{DECODER_ALL_BITS, 0x3f1fff, opcode_reti},
	{DECODER_ALL_BITS, 0x3ebfff, opcode_pop},
	{0x3f0000, 0x3a0000, opcode_calls_imm},
	{0x3f0000, 0x390000, opcode_call_imm},
	{0x380000, 0x300000, opcode_jump_imm},
	{DECODER_ALL_BITS, 0x2dffff, opcode_push},
	{DECODER_ALL_BITS, 0x2affff, opcode_calls_ip},
	{DECODER_ALL_BITS, 0x29ffff, opcode_call_ip},
	{0x38ffff, 0x20ffff, opcode_jump_ip},
	{0x380000, 0x180000, opcode_alu1},
	{0x380000, 0x100000, opcode_alu2},
	{0x3f0000, 0x0e0000, opcode_alu3},
	{0x3e0000, 0x0c0000, opcode_alu4},
	{0x3ffeff, 0x0beeff, opcode_pmd},
	{DECODER_ALL_BITS, 0x0bdfff, opcode_halt},
	{0x3ffff0, 0x0bbff0, opcode_freq},
	{DECODER_ALL_BITS, 0x0b7fff, opcode_sflag},
	{0x3e00fc, 0x0600fc, opcode_alu5},
	{0x3e0000, 0x040000, opcode_alu6},
	{0x3fc0ff, 0x0380ff, opcode_move7},
	{0x3fc000, 0x034000, opcode_move8},
	{0x3fc000, 0x02c000, opcode_move9},
	{0x3ff000, 0x01b000, opcode_move10},
	{0x3f0000, 0x000000, opcode_move11},
	{0, 0, opcode_inval},	//stopper last
};

u16 opcode_decode(struct opcode_word opcode)
{
	unsigned i = 0;
	u16 curr_pc = 0;	//reset if bug?

	while(1) {
		if ((opcode.raw & cr816[i].and_mask) == cr816[i].equal) {
			curr_pc = cr816[i].opcode_fcn(opcode);

// 			if (cr816[i].and_mask == 0)
				//always breaks
			break;
		}
		i++;
	}

	return curr_pc;
}

struct opcode_word read_code(u16 addr)
{
	struct opcode_word ret;

	ret = mem_prog[addr];

	return ret;
}

void init_isa(char * filename)
{
	memset(hw_stack, 0, STACK_LEN*2);

	for (unsigned addr = 0; addr < MEM_PROG_LEN; addr++) {
 		mem_prog[addr].raw = 0x3fffff;	//NOP
//		mem_prog[addr].raw = 0x0bdfff;	//halt
	}

	if (filename) {
		FILE * fp;
		struct opcode_word word;
		unsigned addr = 0;
		fp = fopen(filename, "r");
		if (fp == NULL) {
			perror("code dump opening");
			exit(1);
		}

		while (fread(&word.raw, 3, 1, fp) == 1) {
			mem_prog[addr] = word;
			addr++;
		}
		fclose(fp);
	}


	//just validations vvv
#if 0
//opcodes validation (ADD)
unsigned addr=0;
mem_prog[addr++].raw = 0x0ead00 | ((~0x0f)&0xff);
mem_prog[addr++].raw = 0x0ecd00 | ((~0xf6)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x43)&0xff);
mem_prog[addr++].raw = 0x0ecd00 | ((~0x42)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0xff)&0xff);
mem_prog[addr++].raw = 0x0ecd00 | ((~0x01)&0xff);
mem_prog[addr++].raw = 0x0bdfff;
#endif

#if 0
//opcodes validation (CMP)
unsigned addr=0;
mem_prog[addr++].raw = 0x0ead00 | ((~0x50)&0xff);
mem_prog[addr++].raw = 0x0e1d00 | ((~0x62)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x50)&0xff);
mem_prog[addr++].raw = 0x0e1d00 | ((~0x99)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x50)&0xff);
mem_prog[addr++].raw = 0x0e1d00 | ((~0x50)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x50)&0xff);
mem_prog[addr++].raw = 0x0e1d00 | ((~0x47)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0xb6)&0xff);
mem_prog[addr++].raw = 0x0e1d00 | ((~0xb4)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x7e)&0xff);
mem_prog[addr++].raw = 0x0e1d00 | ((~0x80)&0xff);

mem_prog[addr++].raw = 0x0bdfff;
#endif

#if 0
//opcodes validation (CMPA)
unsigned addr=0;
mem_prog[addr++].raw = 0x0ead00 | ((~0x50)&0xff);
mem_prog[addr++].raw = 0x0e0d00 | ((~0x62)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x50)&0xff);
mem_prog[addr++].raw = 0x0e0d00 | ((~0x50)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x50)&0xff);
mem_prog[addr++].raw = 0x0e0d00 | ((~0x47)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x50)&0xff);
mem_prog[addr++].raw = 0x0e0d00 | ((~0x99)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x90)&0xff);
mem_prog[addr++].raw = 0x0e0d00 | ((~0x82)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x90)&0xff);
mem_prog[addr++].raw = 0x0e0d00 | ((~0x90)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x90)&0xff);
mem_prog[addr++].raw = 0x0e0d00 | ((~0xa7)&0xff);

mem_prog[addr++].raw = 0x0ead00 | ((~0x90)&0xff);
mem_prog[addr++].raw = 0x0e0d00 | ((~0x05)&0xff);


mem_prog[addr++].raw = 0x0bdfff;
#endif

#if 0
//opcodes validation
unsigned addr=0;


// ============== subsc
mem_prog[addr++].raw = 0x0eae00;	//move r0,ff
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x77)&0xff);
mem_prog[addr++].raw = 0x0e7d00 | ((~0x07)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eaeff;	//move r0,0
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x77)&0xff);
mem_prog[addr++].raw = 0x0e7d00 | ((~0x07)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eae00;	//move r0,ff
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x07)&0xff);
mem_prog[addr++].raw = 0x0e7d00 | ((~0x77)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eaeff;	//move r0,0
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x07)&0xff);
mem_prog[addr++].raw = 0x0e7d00 | ((~0x77)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eaeff;	//move r0,0
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0xc6)&0xff);
mem_prog[addr++].raw = 0x0e7d00 | ((~0x5a)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eae00;	//move r0,ff
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x6c)&0xff);
mem_prog[addr++].raw = 0x0e7d00 | ((~0xa5)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0bdfff;	//halt

// ============== subdc
mem_prog[addr++].raw = 0x0eae00;	//move r0,ff
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x77)&0xff);
mem_prog[addr++].raw = 0x0e5d00 | ((~0x07)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eaeff;	//move r0,0
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x77)&0xff);
mem_prog[addr++].raw = 0x0e5d00 | ((~0x07)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eae00;	//move r0,ff
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x07)&0xff);
mem_prog[addr++].raw = 0x0e5d00 | ((~0x77)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eaeff;	//move r0,0
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x07)&0xff);
mem_prog[addr++].raw = 0x0e5d00 | ((~0x77)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eae00;	//move r0,ff
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0xa5)&0xff);
mem_prog[addr++].raw = 0x0e5d00 | ((~0x6c)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0eae00;	//move r0,ff
mem_prog[addr++].raw = 0x0d9fee;	//cpl2 r0
mem_prog[addr++].raw = 0x0ead00 | ((~0x5a)&0xff);
mem_prog[addr++].raw = 0x0e5d00 | ((~0xc6)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0bdfff;	//halt

// ============== subs
mem_prog[addr++].raw = 0x0ead00 | ((~0x56)&0xff);
mem_prog[addr++].raw = 0x0e3d00 | ((~0x12)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0ead00 | ((~0x56)&0xff);
mem_prog[addr++].raw = 0x0e3d00 | ((~0x90)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0ead00 | ((~0x12)&0xff);
mem_prog[addr++].raw = 0x0e3d00 | ((~0x56)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0ead00 | ((~0x90)&0xff);
mem_prog[addr++].raw = 0x0e3d00 | ((~0x56)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0bdfff;	//halt

// ============== subd
mem_prog[addr++].raw = 0x0ead00 | ((~0x56)&0xff);
mem_prog[addr++].raw = 0x0e4d00 | ((~0x12)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0ead00 | ((~0x56)&0xff);
mem_prog[addr++].raw = 0x0e4d00 | ((~0x90)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0ead00 | ((~0x12)&0xff);
mem_prog[addr++].raw = 0x0e4d00 | ((~0x56)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0ead00 | ((~0x90)&0xff);
mem_prog[addr++].raw = 0x0e4d00 | ((~0x56)&0xff);
mem_prog[addr++].raw = 0x3fffff;	//nop

mem_prog[addr++].raw = 0x0bdfff;
#endif


}
