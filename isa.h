#ifndef ISA_H
#define ISA_H

#include "disasm.h"

const char * get_ixs_name(enum alu_ixs ixs);
const char * get_regs_name(enum alu_regs regs);

void do_push(u16 val);
u16 do_pop(void);

u16 opcode_decode(struct opcode_word opcode);
struct opcode_word read_code(u16 addr);

void init_isa(char * filename);

#endif
