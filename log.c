/*
 * TODO TODO TODO TODO TODO TODO TODO
 *
 * move reg dump here to be more consistent
 *
 */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "disasm.h"


/** ************* log functions ***********/

//strings
#define LOG_ADDR_STR_MAX 8
static char log_addr_str[LOG_ADDR_STR_MAX+1];

#define LOG_OPCODE_STR_MAX 10
static char log_opcode_str[LOG_OPCODE_STR_MAX+1];

#define LOG_INSTR_NAME_STR_MAX 10
static char log_instr_name_str[LOG_INSTR_NAME_STR_MAX+1];

#define LOG_INSTR_ARGS_STR_MAX 30
static char log_instr_args_str[LOG_INSTR_ARGS_STR_MAX+1];

#define LOG_ACCESS_STR_MAX 80
static char log_access_read_str[LOG_ACCESS_STR_MAX+1];
static unsigned log_access_read_idx = 0;
static char log_access_write_str[LOG_ACCESS_STR_MAX+1];
static unsigned log_access_write_idx = 0;

#define LOG_COMMENT_STR_MAX 1000
static char log_comment_str[LOG_COMMENT_STR_MAX+1];
static unsigned log_comment_idx = 0;

//functions
void log_addr(u16 addr)
{
	snprintf(log_addr_str, LOG_ADDR_STR_MAX, "%04hx", addr);
}

void log_opcode(struct opcode_word opcode)
{
	snprintf(log_opcode_str, LOG_OPCODE_STR_MAX, "%06x", opcode.raw);
}

void log_instr_name(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vsnprintf(log_instr_name_str,
		LOG_INSTR_NAME_STR_MAX,
		fmt,
		args
	);
	va_end(args);
}

void log_instr_args(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vsnprintf(log_instr_args_str,
		LOG_INSTR_ARGS_STR_MAX,
		fmt,
		args
	);
	va_end(args);
}

//TODO use trace
void log_access_read_add(const char *fmt, ...)
{
	if (log_access_read_idx < LOG_ACCESS_STR_MAX) {
		va_list args;
		va_start(args, fmt);
		log_access_read_idx += vsnprintf(
			&log_access_read_str[log_access_read_idx],
			LOG_ACCESS_STR_MAX - log_access_read_idx,
			fmt,
			args
		);
		va_end(args);
	}
}

void log_access_write_add(const char *fmt, ...)
{
	if (log_access_write_idx < LOG_ACCESS_STR_MAX) {
		va_list args;
		va_start(args, fmt);
		log_access_write_idx += vsnprintf(
			&log_access_write_str[log_access_write_idx],
			LOG_ACCESS_STR_MAX - log_access_write_idx,
			fmt,
			args
		);
		va_end(args);
	}
}

void log_comment_add(const char *fmt, ...)
{
	if (log_comment_idx < LOG_COMMENT_STR_MAX) {
		va_list args;
		va_start(args, fmt);
		log_comment_idx += vsnprintf(
			&log_comment_str[log_comment_idx],
			LOG_COMMENT_STR_MAX - log_comment_idx,
			fmt,
			args
		);
		va_end(args);
	}
}


void log_comment_reg_bits(
	const char *caption,
	const char *reg_names[],
	u8 old,
	u8 new)
{
	u8 diff = old ^ new;

	if (diff) {
		log_comment_add("	%s(%02hhx->%02hhx)\n", caption, old, new);
	} else {
		log_comment_add("	%s(%02hhx)\n", caption, new);
	}

	log_comment_add("	");
	for(unsigned bit=0; bit<8; bit++) {
		log_comment_add(" %1d:%s",
			7-bit,
			reg_names[7-bit]
		);
	}
	log_comment_add("\n");


	log_comment_add("	");
	for(unsigned bit=0; bit<8; bit++) {
		if (diff & (1 << (7-bit))) {
			log_comment_add(" %.*s%c->%c",
				strlen(reg_names[7-bit])-2,
				"                      ",
				old & (1<<(7-bit))?'1':'0',
				new & (1<<(7-bit))?'1':'0'
			);
		} else {
			log_comment_add(" %.*s%c",
				strlen(reg_names[7-bit])+1,
				"                      ",
				new & (1<<(7-bit))?'1':'0'
			);
		}
	}
	log_comment_add("\n");
}

///main flush

void log_flush(unsigned flags)
{
	//addr opbytes name+args comments

	if (flags) {
		printf("%4s:\t%s    %s %-*s\n",
			log_addr_str,
			log_opcode_str,
			log_instr_name_str,
			25-(int)strlen(log_instr_name_str),
			log_instr_args_str
		);
	} else {
		//TODO split, no W, no R (don't do if trace log will be used)
		printf("%4s:\t%s    %s %-*s ;R{%s} ;W{%s}\n",
			log_addr_str,
			log_opcode_str,
			log_instr_name_str,
			25-(int)strlen(log_instr_name_str),
			log_instr_args_str,
			log_access_read_str,
			log_access_write_str
		);
	}

	if (strlen(log_comment_str)) {
// 		printf("\n%s\n", log_comment_str);
 		printf("%s", log_comment_str);
	}

	log_addr_str[0] = '\0';
	log_opcode_str[0] = '\0';
	log_instr_name_str[0] = '\0';
	log_instr_args_str[0] = '\0';
	log_access_read_str[0] = '\0';
	log_access_write_str[0] = '\0';
	log_comment_str[0] = '\0';

	log_access_read_idx = 0;
	log_access_write_idx = 0;
	log_comment_idx = 0;
}

extern u8 read_mem(u16 addr, unsigned flags);

#define CHAR_PER_LINE 16
void log_buf(char *name, u16 base, u16 len)
{
	//TODO use log_comment buffer? problem with really long memdumps (limited buffer)
	printf("%s:\n", name);

	for(unsigned off=0;off<len;off++){
		if ((off%CHAR_PER_LINE) == 0) {
			printf("%04hx: ", off);
		}

		printf("%02hhx ", read_mem(base+off, 0));

		if ((off%CHAR_PER_LINE) == (CHAR_PER_LINE-1)) {
			printf(" ");
			u8 val;
			for(unsigned charoff=0;charoff<CHAR_PER_LINE;charoff++){
				val = read_mem(base + charoff + (off/CHAR_PER_LINE)*CHAR_PER_LINE, 0);

				printf("%c", ((val>=32)&&(val<=128))?val:'.');
			}
			printf("\n");
		}
	}
}
