#ifndef LOG_H
#define LOG_H

#include "disasm.h"

void log_addr(u16 addr);
void log_opcode(struct opcode_word opcode);
void log_instr_name(const char *fmt, ...);
void log_instr_args(const char *fmt, ...);
void log_access_read_add(const char *fmt, ...);
void log_access_write_add(const char *fmt, ...);
void log_comment_add(const char *fmt, ...);
// void log_comment_reg_bits(char *caption, const char *reg_names[], u8 val);
// void log_comment_reg_bits_diff(char *caption, const char *reg_names[], u8 old, u8 new);
void log_comment_reg_bits(
	const char *caption,
	const char *reg_names[],
	u8 old,
	u8 new);

void log_flush(unsigned flags);
void log_buf(char *name, u16 base, u16 len);

#endif
