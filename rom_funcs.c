/*
 * TODO TODO TODO TODO TODO TODO TODO TODO TODO
 *
 * some ROM functions are not completely implemented nor documented
 * emulate more of smb access with fifo, rom functions emulate
 * argument pointer evaluate location
 *
 */


#include <stdio.h>
#include "log.h"
#include "disasm.h"
#include "access.h"
#include "isa.h"

//for SMB input from user HACK
extern u8 * test_buf(void);

#define r_r3 read_reg8(MAIN_REG_R3, 1)
#define r_r2 read_reg8(MAIN_REG_R2, 1)
#define r_r1 read_reg8(MAIN_REG_R1, 1)
#define r_r0 read_reg8(MAIN_REG_R0, 1)

#define w_r3(x) write_reg8(MAIN_REG_R3, x, 1)
#define w_r2(x) write_reg8(MAIN_REG_R2, x, 1)
#define w_r1(x) write_reg8(MAIN_REG_R1, x, 1)
#define w_r0(x) write_reg8(MAIN_REG_R0, x, 1)

#define r_st(x) read_mem(stack_ptr + (x), 1)
#define w_st(x, y) write_mem(stack_ptr + (x), y, 1)

/** ********* rom routines ************/

static u16 rom_execute(u16 stack_ptr)
{
	//TODO code analysis
	log_comment_add("	void? rom_execute(void?)	//untested\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

static u16 smbSlaveCmd(u16 stack_ptr)
{
	u8 cmd = r_r3;
	u8 size = r_r2;
	u16 table_ptr = (r_r1 << 8) | r_r0;
	u16 ret;

	log_comment_add(
		"	int smbSlaveCmd(\n"
		"		u8 cmd  = %02hhx,\n"
		"		u8 size = %hhu,\n"
		"		unsigned *table() = %04hx)\n",
		cmd,
		size,
		table_ptr
	);

	if (cmd < size) {
		write_ip(read_pc()+1, 1);

//TODO ?need modify rom code, perma hook?
		ret = table_ptr + cmd;

//cannot be set registers will be changed in jumped functions (maybe use footer detection :-/ )
// 		reg[3] = 0;	//ret success
// 		reg[2] = 1;	//ret success
	} else {
		w_r3(0);	//ret failed
		w_r2(0);	//ret failed

		ret = read_ip(1);
	}

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return ret;
}

static u16 smb_NACK(u16 stack_ptr)
{
	log_comment_add("	void smb_NACK(void)\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

static u16 smb_ACK(u16 stack_ptr)
{
	log_comment_add("	void smb_ACK(void)\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


// u8 smb_data_val=0x71;

static u16 smbSlaveRcvWord(u16 stack_ptr)
{
	u16 data = (r_r3 << 8) | r_r2;

	log_comment_add(
		"	int smbSlaveRcvWord(int *data = [%04hx])\n",
		data
	);

	//testing NOTICE
	u16 val;
// 	val = 0x1402;
//	val = 0xfd34;
	val = test_buf()[0] << 8;
	val |= test_buf()[0];

	log_comment_add("	VVV %04hx\n", val);

// 	smb_data_val = 0x73;

	write_mem(data, val & 0xff, 1);
	write_mem(data+1, (val >> 8) & 0xff, 1);


	log_comment_add("	XXX %02hhx %02hhx\n",
					read_mem(data, 0),
					read_mem(data+1, 0)

   				);

	w_r3(0);	//ret success
	w_r2(1);	//ret success

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

static u16 smbSlaveSndWord(u16 stack_ptr)
{
	u16 data = (r_r3 << 8) | r_r2;

	log_comment_add(
		"	int smbSlaveSndWord(int data = %04hx)\n",
		data
	);

	w_r3(0);	//ret success
	w_r2(1);	//ret success

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


static u16 smbSlaveSndBlock(u16 stack_ptr)
{
	u8 cnt = r_r3;
	u16 dptr = (r_r2 << 8) | r_r1;

	log_comment_add(
		"	void smbSlaveSndBlock(\n"
		"		u8 byte_cnt = %hhu,\n"
		"		u8 *block   = [%04hx])\n",
		cnt,
		dptr
	);

	log_comment_add("\n");
	log_comment_add("	Data: {");
	u8 data;
	for (unsigned a=0;a<cnt;a++) {
		data = read_mem(dptr + a, 1);
		log_comment_add("%02hhx, ", data);
	}
	log_comment_add("}\n");

	w_r3(0);	//ret success
	w_r2(1);	//ret success

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


static u16 smbSlaveRcvBlock(u16 stack_ptr)
{
	u16 cnt_ptr = (r_r3 << 8) | r_r2;
	u16 dptr = (r_r1 << 8) | r_r0;

	log_comment_add(
		"	void smbSlaveRcvBlock(\n"
		"		u8 *byte_cnt = [%04hx],\n"
		"		u8 *block    = [%04hx])\n",
		cnt_ptr,
		dptr
	);

	u8 cnt = test_buf()[0];
	u8 data;

	write_mem(cnt_ptr, cnt, 1);

	log_comment_add("\n");
	log_comment_add("	Cnt=%hhu\n", cnt);
	log_comment_add("	Data: {");
	for (unsigned a=0;a<cnt;a++) {
		data = test_buf()[0];
		log_comment_add("%02hhx, ", data);
		write_mem(dptr + a, data, 1);
	}
	log_comment_add("}\n");

	w_r3(0);	//ret success
	w_r2(1);	//ret success

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}



static u16 smbSlaveWord(u16 stack_ptr)
{
	u16 dir_ptr = (r_r3 << 8) | r_r2;
	u16 data_ptr = (r_r1 << 8) | r_r0;

	log_comment_add(
		"	int smbSlaveWord(\n"
		"		u8 *dir   = [%04hx],\n"
		"		int *data = [%04hx])\n",
		dir_ptr,
		data_ptr
	);

	log_comment_add("\n");
	log_comment_add("	read pre %02hhx %02hhx\n",
		read_mem(data_ptr, 0),
		read_mem(data_ptr+1, 0)
	);

	//this are changeable values
	u8 flag_written = test_buf()[0];

	write_mem(dir_ptr, flag_written, 1);

	if (flag_written) {
		u8 lo = test_buf()[0];
		u8 hi = test_buf()[0];

		log_comment_add("	write %02hhx %02hhx\n",
			lo, hi
		);

		write_mem(data_ptr, lo, 1);
		write_mem(data_ptr+1, hi, 1);
	}

	w_r3(0);	//ret success
	w_r2(1);	//ret success

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


static u16 smbSlaveBlock(u16 stack_ptr)
{
	u16 drdy_ptr = (r_r3 << 8) | r_r2;
	u16 bytecnt_ptr = (r_r1 << 8) | r_r0;
	u8 maxcnt = r_st(0);
	u16 block_ptr = (r_st(1) << 8) | r_st(2);

	log_comment_add(
		"	int smbSlaveBlock(\n"
		"		u8 *drdy    = [%04hx],\n"
		"		u8 *bytecnt = [%04hx],\n"
		"		u8 maxcnt   = %hhu,\n"
		"		u8 *blocks  = [%04hx])\n",
		drdy_ptr,
		bytecnt_ptr,
		maxcnt,
		block_ptr
	);
	log_comment_add("\n");

	//TODO maxcnt?

	//this are changeable values
	u8 flag_written = test_buf()[0];
	write_mem(drdy_ptr, flag_written, 1);

	u8 bytecnt = read_mem(bytecnt_ptr, 0);
	log_comment_add("	Cnt=%hhu\n", bytecnt);
	log_comment_add("	Data (slave->master): {");
	for (unsigned a=0;a<bytecnt;a++) {
		log_comment_add("%02hhx, ", read_mem(block_ptr + a, 0));
	}
	log_comment_add("}\n");

	if (flag_written) {
		u8 data;
		bytecnt = test_buf()[0];
		log_comment_add("	Cnt=%hhu\n", bytecnt);
		log_comment_add("	Data (master->slave): {");
		for (unsigned a=0;a<bytecnt;a++) {
			data = test_buf()[0];
			log_comment_add("%02hhx, ", data);
			write_mem(block_ptr + a, data, 1);
		}
		log_comment_add("}\n");
	}


	w_r3(0);	//ret success
	w_r2(1);	//ret success

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


#define BQ29330_REG_NUM 9
u8 bq29330[BQ29330_REG_NUM];

void write_bq29330_reg(u8 reg, u8 val)
{
	if (reg >= BQ29330_REG_NUM)
		return;

	bq29330[reg] = val;
}

// #define BQ29330_REG_NAMES_MAX 16

const char *bq29330_reg_names[BQ29330_REG_NUM] = {
	"status",
	"output_ctrl",
	"state_ctrl",
	"fcn_ctrl",
	"cell_sel",
	"olv",
	"old",
	"scc",
	"scd",
};

const char *bq29330_bits_names[BQ29330_REG_NUM][8] = {
	{
		"scd", "scc", "ol", "wdf", "zv", "rsvd5", "rsvd6", "rsvd7"
	},
	{
		"ltclr", "dsg", "chg", "xzv", "gpod", "pms_chg", "rsvd6", "rsvd7"
	},
	{
		"sleep", "ship", "wddis", "wdrst", "rsns", "rsvd5", "rsvd6", "rsvd7"
	},
	{
		"vmen", "pack", "bat", "tout", "rsvd4", "rsvd5", "rsvd6", "rsvd7"
	},
	{
		"cell0", "cell1", "cal0", "cal1", "cb0", "cb1", "cb2", "cb3"
	},
	{
		"olv0", "olv1", "olv2", "olv3", "olv4", "rsvd5", "rsvd6", "rsvd7"
	},
	{
		"old0", "old1", "old2", "old3", "rsvd4", "rsvd5", "rsvd6", "rsvd7"
	},
	{
		"sccv0", "sccv1", "sccv2", "sccv3", "sccd0", "sccd1", "sccd2", "sccd3"
	},
	{
		"scdv0", "scdv1", "scdv2", "scdv3", "scdd0", "scdd1", "scdd2", "scdd3"
	},
};



static u16 I2CWriteBlock(u16 stack_ptr)
{
	u8 addr = r_r3;
	u8 cmd = r_r2;
	u8 cnt = r_r1;
	u16 dptr = (r_st(0) << 8) | r_st(1);

	log_comment_add(
		"	u8 I2CWriteBlock(\n"
		"		u8 addr  = %02hhx,\n"
		"		u8 cmd   = %02hhx,\n"
		"		u8 cnt   = %hhu,\n"
		"		u8 *data = [%04hx])\n",
		addr,
		cmd,
		cnt,
		dptr
	);

	log_comment_add("\n");

	if (1) {
//	if (addr == 0x40) {
		for (u8 idx = 0; idx < cnt; idx++) {

			if ((cmd+idx) < BQ29330_REG_NUM) {
				u8 val_old = bq29330[cmd+idx];
				u8 val_new = read_mem(dptr + idx, 0);

				log_comment_reg_bits(
					bq29330_reg_names[cmd+idx],
					bq29330_bits_names[cmd+idx],
					val_old,
					val_new
				);

				bq29330[cmd+idx] = val_new;

				if (((cmd+idx)==1) && (val_new&1)) {
					//unlatch errors
					bq29330[0]=0;
				}


			} else {
				log_comment_add("	unknown bq29330[%hhd]=%02hhx\n",
								cmd+idx,
								read_mem(dptr + idx, 1));
			}
		}
	} else {
		for (u8 idx = 0; idx < cnt; idx++) {
			log_comment_add("	unknown dev[%hhd]=%02hhx\n",
							idx,
							read_mem(dptr + idx, 1));
		}
	}

	w_r3(0);	//ret success
	w_r2(1);	//ret success

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

static u16 I2CReadBlock(u16 stack_ptr)
{

	u8 addr = r_r3;
	u8 cmd = r_r2;
	u8 cnt = r_r1;
	u16 dptr = (r_st(0) << 8) | r_st(1);

	log_comment_add(
		"	u8 I2CReadBlock(\n"
		"		u8 addr  = %02hhx,\n"
		"		u8 cmd   = %02hhx,\n"
		"		u8 cnt   = %hhu,\n"
		"		u8 *data = [%04hx])\n",
		addr,
		cmd,
		cnt,
		dptr
	);

	log_comment_add("\n");

	if (1) {
//	if (addr == 0x40) {
		for (u8 idx = 0; idx < cnt; idx++) {

			if ((cmd+idx) < BQ29330_REG_NUM) {
				u8 val = bq29330[cmd + idx];

				log_comment_reg_bits(
					bq29330_reg_names[cmd+idx],
					bq29330_bits_names[cmd+idx],
					val,
					val
				);

				write_mem(dptr + idx, val, 1);
			} else {
				log_comment_add("	unknown bq29330[%hhd]=%02hhx\n",
								cmd+idx,
								read_mem(dptr + idx, 1));
			}
		}
	} else {
		for (u8 idx = 0; idx < cnt; idx++) {
			log_comment_add("	unknown dev[%hhd]=%02hhx\n",
							idx,
							read_mem(dptr + idx, 1));
		}
	}

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	w_r3(0);	//ret success
	w_r2(1);	//ret success

	return read_ip(1);
}


static u16 FdataEraseRow(u16 stack_ptr)
{
	u8 row = r_r3;
	u16 flash_addr = 32*row + 0x4000;

	log_comment_add("	void FdataEraseRow(u8 row = %hhd)\n",
		row
	);

	log_comment_add("\n");
	log_comment_add("	FLASH[%04hx]\n",
		flash_addr
	);

	//NOTICE erases 2 rows
	for (unsigned a=0;a<64;a++) {
		write_mem(flash_addr + a, 0xff, 1);
	}

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


static u16 FdataProgRow(u16 stack_ptr)
{
	u8 row = r_r3;
	u8 col = r_r2;
	u8 cnt = r_r1;

	u16 flash_addr = 32*row + col + 0x4000;
	u16 dptr = (r_st(0) << 8) | r_st(1);

	log_comment_add(
		"	void FdataProgRow(\n"
		"		u8 row   = %hhd,\n"
		"		u8 col   = %hhd,\n"
		"		u8 cnt   = %hhd,\n"
		"		u8 *data = [%04hx])\n",
		row,
		col,
		cnt,
		dptr
	);

	log_comment_add("\n");
	log_comment_add("	FLASH[%04hx]\n",
		flash_addr
	);

	log_comment_add("	Data: {");
	u8 data;
	for (unsigned a=0;a<cnt;a++) {
		data = read_mem(dptr + a, 1);
		log_comment_add("%02hhx, ", data);
		write_mem(flash_addr + a, data, 1);
	}
	log_comment_add("}\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

static u16 FdataProgWord(u16 stack_ptr)
{
	u16 flash_addr = (r_r3 << 8) | r_r2;
	u8 data = r_r1;

	log_comment_add(
		"	void FdataProgWord(\n"
		"		u8 *addr = [%04hx],\n"
		"		u8 data  = %02hhx)\n",
		flash_addr,
		data
	);

	write_mem(flash_addr, data, 1);

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

//program memory, read only LSB u16
static u16 FlashRdRow(u16 stack_ptr)
{
	u16 row = (r_r3 << 8) | r_r2;
	u8 col = r_r1;
	u8 cnt = r_r0;
	u16 dataptr = (r_st(0) << 8) | r_st(1);

	log_comment_add(
		"	void FlashRdRow(\n"
		"		u16 row   = %hu,\n"
		"		u8 col    = %hhd,\n"
		"		u8 cnt    = %hhd,\n"
		"		u16 *data = [%04hx])\n",
		row, col, cnt, dataptr
	);

	u16 prog_addr = row*32+col;
	struct opcode_word prog_word = read_code(prog_addr);

	log_comment_add("	code[%04hx]=%06x\n",
		prog_addr, prog_word.raw
	);

	//TODO missing cnt > 1 (implement!)
	write_mem(dataptr + 0, (prog_word.raw >> 8) & 0xff, 1);
	write_mem(dataptr + 1, (prog_word.raw >> 0) & 0xff, 1);

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


static u16 FdataMassErase(u16 stack_ptr)
{
	log_comment_add("	void FdataMassErase(void)\n");

	for (unsigned a=0x4000;a<0x4800;a++) {
		write_mem(a, 0xff, 0);	//NOTICE, that would be hell to log
	}

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


static u16 SetAddr(u16 stack_ptr)
{
	log_comment_add("	void SetAddr(void)\n");
	log_comment_add("		fully transparent\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

static u16 PokeByte(u16 stack_ptr)
{
	log_comment_add("	void PokeByte(void)\n");
	log_comment_add("		fully transparent\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

static u16 PeekByte(u16 stack_ptr)
{
	log_comment_add("	void PeekByte(void)\n");
	log_comment_add("		fully transparent\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


static u16 ReadRAMBlk(u16 stack_ptr)
{
	log_comment_add("	void ReadRAMBlk(void)\n");
	log_comment_add("		fully transparent\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}

static u16 smbWaitBusFree(u16 stack_ptr)
{
	u8 status = r_r3;

	log_comment_add("	int smbWaitBusFree(u8 status = %hhd)\n",
		status
	);

	w_r3(0);	//ret success
	w_r2(1);	//ret success

// 	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


//compiler fcns

static u16 mulhisi3(u16 stack_ptr)
{
	s16 a = (r_r3 << 8) | r_r2;
	s16 b = (r_r1 << 8) | r_r0;

	log_comment_add(
		"	s32 mulhisi3(\n"
		"		s16 a = %hi,\n"
		"		s16 b = %hi)\n",
		a, b
	);

	s32 m = a * b;

	log_comment_add("\n");
	log_comment_add("	mul=%li\n", m);

	w_r3((m >> 24) & 0xff);
	w_r2((m >> 16) & 0xff);
	w_r1((m >> 8) & 0xff);
	w_r0((m >> 0) & 0xff);

// 	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return do_pop();
}

static u16 umulhisi3(u16 stack_ptr)
{
	u16 a = (r_r3 << 8) | r_r2;
	u16 b = (r_r1 << 8) | r_r0;

	log_comment_add(
		"	u32 umulhisi3(\n"
		"		u16 a = %hu,\n"
		"		u16 b = %hu)\n",
		a, b
	);

	u32 m = a * b;

	log_comment_add("\n");
	log_comment_add("	mul=%u\n", m);

	w_r3((m >> 24) & 0xff);
	w_r2((m >> 16) & 0xff);
	w_r1((m >> 8) & 0xff);
	w_r0((m >> 0) & 0xff);

	return do_pop();
}


static u16 mulsi3(u16 stack_ptr)
{
	u32 a;
	a = r_st(0) << 24;
	a |= r_st(1) << 16;
	a |= r_st(2) << 8;
	a |= r_st(3) << 0;

	u32 b;
	b = r_st(4) << 24;
	b |= r_st(5) << 16;
	b |= r_st(6) << 8;
	b |= r_st(7) << 0;

	log_comment_add(
		"	u32 mulsi3(\n"
		"		u32 a = %u,\n"
		"		u32 b = %u)\n",
		a, b
	);

	u64 m = a * b;

	log_comment_add("\n");
	log_comment_add("	mul=%lu\n", m);

	w_r3((m >> 24) & 0xff);
	w_r2((m >> 16) & 0xff);
	w_r1((m >> 8) & 0xff);
	w_r0((m >> 0) & 0xff);

	return do_pop();
}

static u16 udivmodhi4(u16 stack_ptr)
{
	u16 a = (r_r3 << 8) | r_r2;
	u16 b = (r_st(0) << 8) | r_st(1);

	log_comment_add(
		"	u32 udivmodhi4(\n"
		"		u16 a = %hu,\n"
		"		u16 b = %hu)\n",
		a, b
	);

	u16 q = a / b;
	u16 r = a % b;

	log_comment_add("\n");
	log_comment_add("	rem=%hu quot=%hu\n", r, q);

	w_r3((r >> 8) & 0xff);
	w_r2((r >> 0) & 0xff);
	w_r1((q >> 8) & 0xff);
	w_r0((q >> 0) & 0xff);

	return do_pop();
}

static u16 divmodsi4(u16 stack_ptr)
{
	u32 a;
	a = (r_r3 << 24) & 0xff;
	a |= (r_r2 << 16) & 0xff;
	a |= (r_r1 << 8) & 0xff;
	a |= (r_r0 << 0) & 0xff;

	u32 b;
	b = r_st(0) << 24;
	b |= r_st(1) << 16;
	b |= r_st(2) << 8;
	b |= r_st(3) << 0;

	log_comment_add(
		"	u32 divmodsi4(\n"
		"		u32 a = %u,\n"
		"		u32 b = %u)\n",
		a, b
	);

	u32 q = a / b;
	u32 r = a % b;

	log_comment_add("\n");
	log_comment_add("	rem=%u quot=%u\n", r, q);

	w_r3((q >> 24) & 0xff);
	w_r2((q >> 16) & 0xff);
	w_r1((q >> 8) & 0xff);
	w_r0((q >> 0) & 0xff);

	w_st(0, (r >> 24) & 0xff);
	w_st(1, (r >> 16) & 0xff);
	w_st(2, (r >> 8) & 0xff);
	w_st(3, (r >> 0) & 0xff);

	return do_pop();
}

static u16 divmodhi4(u16 stack_ptr)
{
	s16 a = (r_r1 << 8) | r_r0;
	s16 b = (r_st(0) << 8) | r_st(1);

	log_comment_add(
		"	s32 divmodhi4(\n"
		"		s16 a = %hi,\n"
		"		s16 b = %hi)\n",
		a, b
	);

	s16 q = a / b;
	s16 r = a % b;

	log_comment_add("\n");
	log_comment_add("	rem=%hi quot=%hi\n", r, q);

	w_r3((r >> 8) & 0xff);
	w_r2((r >> 0) & 0xff);
	w_r1((q >> 8) & 0xff);
	w_r0((q >> 0) & 0xff);

	return do_pop();
}

////////weird stuff

static u16 IrqROMHandler(u16 stack_ptr)
{
	log_comment_add("	void IrqROMHandler(void)\n");
	log_comment_add("		unknown function, maybe rom irq handler, where to return?\n");

	sim_breakpoint_set(SIM_BREAKPOINT_CODE);

	return read_ip(1);
}


//TODO maybe bitfield of input params, output params
//will be limited to 64 probably (u64 bitfield)
#define FCN_OSTACK		(1U<<0)
#define FCN_ISTACK(x)	((x)<<1)

struct rom_fcn {
	u16 pc;
	u16 (*fcn_call)(u16);
	unsigned flags;
};

static const struct rom_fcn fcns[] = {
	{0x8004, rom_execute,		0},
// 	{0x8005, smbMasterWrWord,	0},
 	{0x8009, smbSlaveCmd,		0},
	{0x800a, smbSlaveRcvWord,	0},
	{0x800b, smbSlaveSndWord,	0},
	{0x800c, smbSlaveSndBlock,	0},
	{0x800d, smbSlaveRcvBlock,	0},
	{0x800e, smbSlaveWord,		0},
	{0x800f, smbSlaveBlock,		FCN_ISTACK(3)},
	{0x8012, smb_ACK,		0},
	{0x8013, smb_NACK,		0},
 	{0x8014, FlashRdRow,	FCN_ISTACK(2)},
 	{0x8017, SetAddr,		0},		//calls+jmp
	{0x8018, PokeByte,		0},		//calls+jmp
	{0x8019, PeekByte,		0},		//calls+jmp
	{0x801a, ReadRAMBlk,	0},		//calls+jmp
	//math
	{0x801c, mulhisi3,		0},
	{0x801d, umulhisi3,		0},
	{0x801e, mulsi3,		FCN_ISTACK(8)},
	{0x8020, divmodhi4,		FCN_ISTACK(2)},
	{0x8021, udivmodhi4,	FCN_ISTACK(2)},
	{0x8022, divmodsi4,		FCN_ISTACK(4) | FCN_OSTACK},
	//more fcn
	{0x8037, FdataProgRow,		FCN_ISTACK(2)},
	{0x8038, FdataProgWord,		0},
	{0x8039, FdataEraseRow,		0},
 	{0x803a, FdataMassErase,	0},
	{0x803b, I2CReadBlock,		FCN_ISTACK(2)},
	{0x803c, I2CWriteBlock,		FCN_ISTACK(2)},
// 	{0x803d, I2CDeviceAvail,	0},
// 	{0x803e, I2CCompareBlock,	0},
	{0x8043, smbWaitBusFree,	0},
//
// 	//found calls
// 	{0x8040, rsvd/tramp?/irq6+},
	{0x8040, IrqROMHandler,		0},

	//last entry, just dump stack
	{0, NULL, FCN_ISTACK(8)},
};


/*
 * call ABI
 *
 * args: r3, r2, r1, r0, stack 0, stack 1, 2 3 4...
 * (i3 = stack)
 * rets: r2 (lsb), r3 (msb)
 *
 * NOTICE R0 can be skipped if u16 not aligned?
 */

u16 check_rom_funcs(u16 pc)
{
	if (pc < 0x8000)
		return pc;

	log_comment_add("=== ROM routines: ===\n");

	unsigned flag_hit = 0;
	u16 sw_stack_ptr = read_ix(I_REG_3, 1);
	unsigned idx = 0;

	log_comment_add("	Input: r3=%02hhx r2=%02hhx r1=%02hhx r0=%02hhx ",
		read_reg8(MAIN_REG_R3, 0),
		read_reg8(MAIN_REG_R2, 0),
		read_reg8(MAIN_REG_R1, 0),
		read_reg8(MAIN_REG_R0, 0)
	);


	idx = 0;
	while (1) {
		if ((fcns[idx].pc == pc) || (fcns[idx].pc == 0)) {
			for (
				unsigned sti=0;
				sti<((fcns[idx].flags >> 1) & 0xf);
				sti++)
			{
				log_comment_add("st[%i]=%02hhx ",
					sti, read_mem(sw_stack_ptr + sti, 0)
				);
			}
			log_comment_add("\n\n");

			if (fcns[idx].pc == 0)
				break;

			pc = fcns[idx].fcn_call(sw_stack_ptr);

			//changed
			log_comment_add("\n");
			log_comment_add("	Output: r3=%02hhx r2=%02hhx r1=%02hhx r0=%02hhx ",
				read_reg8(MAIN_REG_R3, 0),
				read_reg8(MAIN_REG_R2, 0),
				read_reg8(MAIN_REG_R1, 0),
				read_reg8(MAIN_REG_R0, 0)
			);

			if (fcns[idx].flags & 1) {
				for (unsigned sti=0;sti<4;sti++) {
					log_comment_add("st[%i]=%02hhx ",
						sti, read_mem(sw_stack_ptr + sti, 0)
					);
				}
			}
			log_comment_add("\n\n");

			flag_hit = 1;
			break;
		}
		idx++;
	}

	if (flag_hit == 0) {
		log_comment_add("	!!unknown routine @%04hx()\n", pc);

		sim_breakpoint_set(SIM_BREAKPOINT_CODE);

		pc = read_ip(1);
	}

	return pc;
}

