#ifndef ROM_FUNCS_H
#define ROM_FUNCS_H

#include "disasm.h"

u16 check_rom_funcs(u16 pc);

#endif
