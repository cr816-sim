#!/bin/sh
#$(()) should be used
#TODO check bq mode?

SMB_IFACE=$((0))
SMB_ADDR=$((0xb))

SMB_STRING=""

smb_init() {
	SMB_STRING=""
}

#$1 count
#$2- data
smb_write() {
	local CNT="${1}"
	shift
	if [ "${#}" -ne "${CNT}" ] ; then
		echo "invalid count" >&2
		return
	fi

	SMB_STRING="${SMB_STRING} w${CNT}@${SMB_ADDR} ${@}"
}

#$1 count
smb_read() {
	local CNT="${1}"

	SMB_STRING="${SMB_STRING} r${CNT}@${SMB_ADDR}"
}

smb_commit() {
	i2ctransfer -y ${SMB_IFACE} ${SMB_STRING}
}
